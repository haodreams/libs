/*
 * @Author: Wangjun
 * @Date: 2023-07-10 09:14:36
 * @LastEditTime: 2023-07-10 09:22:07
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \libs\hostcode\hsotcode_test.go
 * hnxr
 */
package hostcode

import (
	"log"
	"testing"
)

func TestHost(t *testing.T) {
	code := GetHostCode()
	log.Println(code)
}
