/*
 * @Author: Wangjun
 * @Date: 2023-07-10 09:08:17
 * @LastEditTime: 2025-02-14 10:27:13
 * @LastEditors: wangjun haodreams@163.com
 * @Description:获取机器的硬件码
 * @FilePath: \libs\hostcode\hostcode.go
 * hnxr
 */
package hostcode

import (
	"fmt"
	"net"
	"os"
	"runtime"
	"strings"
	"time"
)

var version = "V1"
var gHardCode = ""

// 版本设置
func SetVersion(ver string) {
	version = ver
}

func GetHostCode(paths ...string) (code string) {
	path := "hostcode"
	if len(paths) > 0 {
		path = paths[0]
	}
	info, err := net.Interfaces()
	if err != nil {
		return getFileCode(code, path)
	}
	cs := make([]string, 0, len(info))
	for _, inf := range info {
		//排除虚拟网卡， 虚拟网卡会变
		if strings.HasPrefix(inf.Name, "dummy") {
			continue
		}
		s := inf.HardwareAddr.String()
		if s != "" {
			s = strings.ReplaceAll(s, ":", "")
			if len(s) == 12 {
				cs = append(cs, encodeMac(s))
			}
		}
	}
	if len(cs) == 0 {
		return getFileCode(code, path)
	}

	data := getFileCode(code, path)
	mac := data[7:]
	for _, s := range cs {
		if s == mac {
			return data
		}
	}
	now := time.Now()
	code = fmt.Sprintf("%s%s-%s%s", version, now.Format("06"), now.Format("01"), cs[0])

	os.WriteFile(path, []byte(code), 0666)
	return
}

func getFileCode(code string, path string) (scode string) {
	if code == "" {
		code = "00" + time.Now().Format("06-0102-1504-") + "0000"
	}
	data, err := os.ReadFile(path)
	if len(data) != 19 || err != nil {
		os.WriteFile(path, []byte(code), 0666)
		return code
	}
	return string(data)
}

func encodeMac(s string) (code string) {
	s = strings.ToUpper(s)
	code = s[2:4] + "-" + s[4:8] + "-" + s[8:12]
	return
}

// 获取默认的硬件码路径
func GetDefaultCodePath() string {
	path := "/etc/hardcode"
	if runtime.GOOS == "windows" {
		path = "hardcode"
	}
	return path
}

// GetHardCode 获取硬件码
func GetHardCode(paths ...string) string {
	if gHardCode != "" {
		return gHardCode
	}
	path := "hardcode"
	if len(paths) > 0 {
		path = paths[0]
	}
	code := time.Now().Format("060102150304")
	getCode := func(code string) string {
		data, err := os.ReadFile(path)
		if err != nil {
			gHardCode = code
			os.WriteFile(path, []byte(code), 0666)
			return code
		}
		return string(data)
	}

	info, err := net.Interfaces()
	if err != nil {
		return getCode(code)
	}
	cs := make([]string, 0, len(info))
	for _, inf := range info {
		//排除虚拟网卡， 虚拟网卡会变
		if strings.HasPrefix(inf.Name, "dummy") {
			continue
		}
		s := inf.HardwareAddr.String()
		if s != "" {
			s = strings.Replace(s, ":", "", -1)
			s = strings.ToUpper(s)
			cs = append(cs, s)
		}
	}
	if len(cs) == 0 {
		gHardCode = code
		return getCode(code)
	}

	data := getCode(code)

	for _, s := range cs {
		if s == data {
			return s
		}
	}
	os.WriteFile(path, []byte(cs[0]), 0666)
	gHardCode = cs[0]
	return cs[0]
}
