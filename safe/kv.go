package safe

type KV[K Key, G any] struct {
	K K
	V *G
}
