/*
 * @Author: Wangjun
 * @Date: 2022-08-09 13:23:56
 * @LastEditTime: 2023-04-06 15:45:45
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \libs\safe\map_any_test.go
 * hnxr
 */
package safe

import (
	"log"
	"testing"
)

type Student struct {
	Name string
	Age  int
}

func TestMapAny(t *testing.T) {
	mapStudent := NewMap[string, Student]()
	s := &Student{Name: "test1", Age: 20}
	mapStudent.Set(s.Name, s)
	s = &Student{Name: "test2", Age: 10}
	mapStudent.Set(s.Name, s)

	k := mapStudent.Get("test2")
	log.Println(k.Name, k.Age)
}

func TestLockMap(t *testing.T) {
	mapStudent := NewSafeMap[string, Student]()
	s := &Student{Name: "test1", Age: 20}
	mapStudent.Set(s.Name, s)
	s = &Student{Name: "test2", Age: 10}
	mapStudent.Set(s.Name, s)

	k, ok := mapStudent.Get("test2")
	log.Println(ok, k.Name, k.Age)

	var p *Student
	p, ok = mapStudent.Get("test1")
	log.Println(ok, p.Name, p.Age)
}
