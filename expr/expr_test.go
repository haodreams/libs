/*
 * @Author: Wangjun
 * @Date: 2023-03-22 09:31:39
 * @LastEditTime: 2023-12-28 18:17:18
 * @LastEditors: wangjun haodreams@163.com
 * @Description:
 * @FilePath: \libs\expr\expr_test.go
 * hnxr
 */
package expr

import (
	"log"
	"testing"
)

func TestExpr(t *testing.T) {
	m := map[string]float64{
		"a1.a2.a3.a4": 10,
		"a3.a4":       5,
	}
	e := New(NewMethod(m))
	// e.RegisterMethod("max", func(v ...float64) (float64, error) {
	// 	if len(v) == 0 {
	// 		return 0, errors.New("参数错误")
	// 	}
	// 	max := v[0]
	// 	for _, x := range v {
	// 		if max < x {
	// 			max = x
	// 		}
	// 	}
	// 	return max, nil
	// })

	err := e.Parse(`If(1<10,a1.a2.a3.a4, a3.a4)`)
	if err != nil {
		log.Fatal(err)
	}

	log.Println(e.Vars())
	v, err := e.Eval()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(v)
}

func TestIn(t *testing.T) {
	ss := "CU in(1 , 7)"
	//ss = "CU in (6,9) || X1 in (96,98,99)"
	// RegisterFunction("in", func(x ...float64) (float64, error) {
	// 	n := len(x)
	// 	if n == 0 {
	// 		return 0, nil
	// 	}
	// 	v := x[0]
	// 	for i := 1; i < n; i++ {
	// 		if v == x[i] {
	// 			return 1, nil
	// 		}
	// 	}
	// 	return 0, nil
	//// })

	ss = ConvertIn(ss, nil)

	e, err := Parse(ss, NewMethod(map[string]float64{"CU": 1}))
	if err != nil {
		log.Fatal(err)
	}
	log.Println("EXPR:", e.String())
	log.Println(e.Vars())
	v, err := e.Eval()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(v)
}
