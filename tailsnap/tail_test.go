package tailsnap

import (
	"log"
	"testing"
)

func TestTail(t *testing.T) {
	lines, err := TailFile("tailsnap.go", 30)
	if err != nil {
		log.Fatal(err)
	}
	//log.Println(lines)
	for _, line := range lines {
		log.Println(*line)
	}
}
