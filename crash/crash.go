/*
 * @Author: Wangjun
 * @Date: 2023-04-07 14:08:16
 * @LastEditTime: 2023-04-07 14:32:20
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \libs\crash\crash.go
 * hnxr
 */
package crash

import (
	"os"

	"gitee.com/haodreams/libs/easy"
)

var f *os.File

func Close() (err error) {
	if f != nil {
		err = f.Close()
		f = nil
	}
	return
}

func Setup(path string) (err error) {
	f, err = os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_APPEND|os.O_SYNC, 0666)
	return
}

func WriteString(line string) (err error) {
	if f == nil {
		return
	}
	f.WriteString(easy.NowWithMill() + "\r\n")
	_, err = f.WriteString(line + "\r\n")
	f.Sync()
	return
}

func Write(line []byte) (err error) {
	if f == nil {
		return
	}
	_, err = f.Write(line)
	f.Sync()
	return
}
