/*
 * @Author: Wangjun
 * @Date: 2022-04-10 17:50:14
 * @LastEditTime: 2023-09-26 09:27:48
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \libs\easy\clock_test.go
 * hnxr
 */
/*
 * @Author: Wangjun
 * @Date: 2022-01-12 16:17:40
 * @LastEditTime: 2022-04-10 17:47:55
 * @LastEditors: Wangjun
 * @Description:日期时间结构体
 * @FilePath: \oneareaprod:\go\src\gitee.com\haodreams\libs\easy\clock.go
 * hnxr
 */

package easy

import (
	"log"
	"testing"
	"time"
)

func TestClock(t *testing.T) {
	now := time.Now()
	_, offset := now.Zone()
	clock := NewClock(now.Unix(), int64(offset))
	log.Println(clock)
	log.Println(FormatTime(clock.today))
}

func TestWeekday(t *testing.T) {
	clock := NewNowClock()
	log.Println(clock)
	log.Println(FormatTime(clock.today), clock.Weekday())
}
