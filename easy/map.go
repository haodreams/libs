/*
 * @Author: Wangjun
 * @Date: 2023-04-14 10:54:23
 * @LastEditTime: 2024-12-12 22:20:47
 * @LastEditors: wangjun haodreams@163.com
 * @Description:
 * @FilePath: \libs\easy\map.go
 * hnxr
 */
package easy

import "sort"

/**
 * @description: 数组转为map
 * @return {*}
 */
func ToMap[T comparable](list []T) (m map[T]int) {
	m = map[T]int{}
	for i, a := range list {
		m[a] = i
	}
	return
}

/**
 * @description: 复制一个map
 * @return {*}
 */
func CopyMap[K comparable, V any](mp map[K]V) (m map[K]V) {
	m = make(map[K]V)
	for k, v := range mp {
		m[k] = v
	}
	return
}

func MapKeys[K comparable, V any](m map[K]V) []K {
	ks := make([]K, len(m))
	i := 0
	for k := range m {
		ks[i] = k
		i++
	}
	return ks
}

func Map2List[K comparable, V any](m map[K]V) []V {
	ks := make([]V, len(m))
	i := 0
	for _, v := range m {
		ks[i] = v
		i++
	}
	return ks
}

type Key interface {
	int64 | int32 | int16 | int8 | uint64 | uint32 | uint16 | uint8 | int | uint | string
}

//map 转排序的数组
func Map2SortList[K Key, V any](m map[K]V) []V {
	type kv struct {
		k K
		v V
	}
	kvs := make([]*kv, len(m))

	i := 0
	for k, v := range m {
		p := new(kv)
		p.k = k
		p.v = v
		kvs[i] = p
		i++
	}
	sort.Slice(kvs, func(i, j int) bool {
		return kvs[i].k < kvs[j].k
	})
	list := make([]V, len(kvs))
	for i, v := range kvs {
		list[i] = v.v
	}
	return list
}
