/*
 * @Author: Wangjun
 * @Date: 2021-12-14 17:06:59
 * @LastEditTime: 2024-12-17 10:39:04
 * @LastEditors: wangjun haodreams@163.com
 * @Description:
 * @FilePath: \libs\easy\string_test.go
 * hnxr
 */
package easy

import (
	"log"
	"math"
	"testing"
	"unicode"
)

func TestFieldN(t *testing.T) {
	line := "2021-12-14 16:45:25.851	INFO	models/redis.go:121 Insert to redis number: 14106  used time: 89.0246ms"
	ss := FieldsFuncN(line, unicode.IsSpace, 4)
	for i := range ss {
		log.Println(ss[i])
	}
}

func TestUID(t *testing.T) {
	a := 0.0
	if true {
		a = math.NaN()
	}
	if a > 0 {
		log.Println("true")
	} else {
		log.Println("false")
	}
	for i := 0; i < 10; i++ {
		log.Println(TID())
	}
}

func TestAppendFile(t *testing.T) {
	AppendFile("test.txt", []byte(Now()+"\n"), 0755)
	AppendFile("test.txt", []byte(Now()+"\n"), 0755)
}
