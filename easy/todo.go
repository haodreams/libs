/*
 * @Author: Wangjun
 * @Date: 2023-03-06 14:22:38
 * @LastEditTime: 2023-03-06 14:23:21
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \libs\easy\todo.go
 * hnxr
 */
package easy

/**
 * @description: 空方法标识后面需要做的工作
 * @param {...interface{}} argvs
 * @return {*}
 */
func Todo(argvs ...any) {}
