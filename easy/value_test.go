/*
 * @Author: Wangjun
 * @Date: 2023-01-06 19:23:25
 * @LastEditTime: 2023-01-06 19:24:44
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \libs\easy\value_test.go
 * hnxr
 */
package easy

import (
	"io"
	"log"
	"testing"
)

func TestIsNil(t *testing.T) {
	aa := make([]io.Reader, 2)
	log.Println(IsNil(aa[0]))
}
