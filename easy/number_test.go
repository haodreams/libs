/*
 * @Author: Wangjun
 * @Date: 2022-08-17 09:54:03
 * @LastEditTime: 2022-08-17 09:57:15
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \libs\easy\number_test.go
 * hnxr
 */
package easy

import (
	"log"
	"testing"
)

func TestRound(t *testing.T) {
	val := 10.1234567
	log.Println(Round(val, 3))
	log.Println(RoundSlow(val, 3))
}
