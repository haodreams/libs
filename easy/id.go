package easy

import (
	"sync/atomic"
	"time"
)

// UID 生成8字节的唯一ID
// 生成15位的短ID
// 后2位作为自定义识别字符
func UID() uint64 {
	const beginTime = 1577808000 //2020-01-01
	now := time.Now().Unix() - int64(beginTime)

	id := now * 1000 * 1000 * 10
	id += int64(atomic.AddUint64(&uid, 1) * 100)
	atomic.StoreUint64(&uid, uid%100000)
	return uint64(id)
}

// GID 生成8字节的唯一ID
// 20位的长ID
// 后2位作为自定义识别字符
func GID() uint64 {
	const ms = 1000 * 1000
	id := time.Now().UnixNano() / ms * ms
	id += int64(atomic.AddUint64(&gid, 1) * 100)
	atomic.StoreUint64(&gid, gid%10000)
	return uint64(id)
}

// TID 生成8字节带日期的唯一ID 每秒最多999999
// 20位的长ID
// 后2位作为自定义识别字符

// 10秒最多有10w个id
func TID() uint64 {
	now := time.Now()

	_, offset := now.Zone()
	sec := now.Unix() + int64(offset)

	year, month, day, _ := AbsDate(sec)
	year %= 10
	sec %= 86400
	sec /= 10

	id := (sec + (int64(day)*10000 + int64(month)*10000*100 + int64(year)*10000*10000)) * 10000000

	id += int64(atomic.AddUint64(&tid, 1) * 100)
	atomic.StoreUint64(&tid, tid%100000)
	return uint64(id)
}
