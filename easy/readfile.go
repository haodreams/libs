/*
 * @Author: Wangjun
 * @Date: 2022-05-16 16:14:05
 * @LastEditTime: 2022-08-17 09:52:41
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \libs\easy\readfile.go
 * hnxr
 */
package easy

import "os"

/**
 * @description: 读取文件数据位字符串
 * @param {string} path
 * @return {*}
 */
func ReadFile(path string) (s string) {
	data, err := os.ReadFile(path)
	if err != nil {
		return
	}
	return string(data)
}
