package easy

import (
	"encoding/hex"
	"log"
	"strings"
	"testing"
)

func TestFixedBuf(t *testing.T) {
	b := NewLoopBuffer(make([]byte, 10))
	b.Write([]byte("abcdefg"))
	buf := make([]byte, 5)
	n, err := b.Read(buf)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println(string(buf[:n]))
	n, err = b.Read(buf)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println(string(buf[:n]))
	_, err = b.Write([]byte("12345678"))
	if err != nil {
		log.Fatalln(err)
	}
	n, err = b.Read(buf)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println(string(buf[:n]))
	n, err = b.Read(buf)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println(string(buf[:n]))
}

// func TestCrc16(t *testing.T) {
// 	data := "01 03 00 00 00 08 44 0C 01 03 10 00 00 16 37 13 6D 27 CD 00 33 00 00 20 00 1B 58"
// 	//data = "11 10 0001 0002 04 000A 0102"
// 	data = strings.Replace(data, " ", "", -1)
// 	bin, err := hex.DecodeString(data)
// 	if err != nil {
// 		log.Println(err)
// 		return
// 	}
// 	var buf ReadBuffer
// 	buf.SetData(bin).Seek(0, io.SeekEnd)

// 	crc, err := buf.CRC16()
// 	c := CRC16(bin)
// 	log.Printf("%x %x %v", c, crc, err)
// }

func TestCrc16_2(t *testing.T) {
	bin := []byte("1d6b3b59-9459-4cdb-8690-c6350f947b4b")
	c := CRC16(bin)
	log.Printf("%x", c)
}

func TestReadWriter(t *testing.T) {
	buf := NewLoopBuffer(make([]byte, 100))
	data := make([]byte, 4)
	data[3] = 100
	a := int8(1)
	b := int16(2)
	c := int64(100)
	d := uint32(90)
	f := 13.94
	buf.WriteAny(a, b, c, d, f, data)
	//buf.Seek(0, 0)
	a = 0
	b = 0
	c = 0
	d = 0
	f = 0
	data[3] = 0
	buf.ReadAny(&a, &b, &c, &d, &f, &data)
	log.Println(a, b, c, d, f, data)
}

// 152 类型，厂家私有协议
func TestTid152(t *testing.T) {
	pack := "68 fe 02 db b4 10 98 01 03 01 01 00 d0 44 00 00 00 00 e0 c3 73 a2 41 b6 6e 14 0a a4 01 13 05 00 00 00 00 32 00 00 00 b1 3d 06 00 b0 3d 06 00 02 23 06 00 01 23 06 00 00 23 06 00 f1 22 06 00 EF 22 06 00 56 ac 04 00 55 ac 04 00 54 ac 04 00 53 ac 04 00 f3 ab 04 00 d4 ab 04 00 a0 ab 04 00 9c ab 04 00 94 ab 04 00 59 ab 04 00 19 a9 04 00 00 a9 04 00 f4 a7 04 00 22 3c 03 00 21 3c 03 00 a9 38 03 00 55 38 03 00 54 38 03 00 52 38 03 00 51 38 03 00 50 38 03 00 4f 38 03 00 40 38 03 00 39 38 03 00 fd 34 03 00 fc 34 03 00 c0 34 03 00 bd 34 03 00 b5 34 03 00 5d 34 03 00 5b 34 03 00 8a 30 03 00 9d 28 03 00 d8 20 03 00 ee 1c 03 00 af 15 03 00 38 15 03 00 31 15 03 00 2b 15 03 00 2a 15 03 00 22 15 03 00 27 9b 01 00 ff ff ff ff dc c9 ff 83 3c 06 08 29 00 00 00 2c 00 00 00 68 8c"
	pack = strings.ReplaceAll(pack, " ", "")
	hexBin, err := hex.DecodeString(pack)
	if err != nil {
		log.Fatalln(err)
	}

	wr := NewBuffer(hexBin[39:])
	wr.SetBigEndian(false)
	for i := 0; i < 50; i++ {
		log.Println(wr.Uint32())
	}
}
