/*
 * @Author: Wangjun
 * @Date: 2021-05-21 14:44:32
 * @LastEditTime: 2023-01-06 19:30:58
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \libs\easy\value.go
 * hnxr
 */
package easy

import (
	"reflect"
	"time"
)

// 返回最新的值
func Newest[V any](vs []V) V {
	return vs[len(vs)-1]
}

/**
 * @description: 判断是否空值
 * @param {interface{}} i
 * @return {*}
 */
func IsNil(i interface{}) bool {
	if i == nil {
		return true
	}
	vi := reflect.ValueOf(i)

	if vi.IsValid() {
		return true
	}
	if vi.Kind() == reflect.Ptr {
		return vi.IsNil()
	}
	//log.Println(vi.Kind().String())
	return false
}

// GetOnlyOneValue 获取唯一的一个值
func GetOnlyOneValue(rows []map[string]interface{}) interface{} {
	if len(rows[0]) == 0 {
		return nil
	}
	for _, value := range rows[0] {
		return value
	}
	return nil
}

// GetOnlyOneTime 仅获取一个时间值
func GetOnlyOneTime(rows []map[string]interface{}) (t time.Time) {
	val := GetOnlyOneValue(rows)
	if val == nil {
		return
	}
	t, _ = val.(time.Time)
	return
}

// GetOnlyOneTime 仅获取一个时间值
func GetOnlyOneFloat64(rows []map[string]interface{}) (f float64) {
	val := GetOnlyOneValue(rows)
	if val == nil {
		return
	}

	switch fval := val.(type) {
	case float64:
		return fval
	case float32:
		return float64(fval)
	case int:
		return float64(fval)
	case int8:
		return float64(fval)
	case int16:
		return float64(fval)
	case int32:
		return float64(fval)
	case int64:
		return float64(fval)
	case uint:
		return float64(fval)
	case uint8:
		return float64(fval)
	case uint16:
		return float64(fval)
	case uint32:
		return float64(fval)
	case uint64:
		return float64(fval)
	}

	return
}
