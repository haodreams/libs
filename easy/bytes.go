/*
 * @Author: Wangjun
 * @Date: 2021-06-01 16:46:34
 * @LastEditTime: 2021-06-01 16:48:12
 * @LastEditors: Wangjun
 * @Description: 自定义数据json的解析
 * @FilePath: \xr_ai_monitord:\go\src\gitee.com\haodreams\libs\easy\bytes.go
 * hnxr
 */
package easy

type Bytes []byte

//UnmarshalJSON 自定义类型序列化
func (m *Bytes) UnmarshalJSON(data []byte) error {
	*m = data
	return nil
}
