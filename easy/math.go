/*
 * @Date: 2022-09-10 15:31:27
 * @LastEditors: Wangjun
 * @LastEditTime: 2023-03-18 22:59:24
 * @FilePath: \libs\easy\math.go
 * @Description:
 */
package easy

type Number interface {
	int64 | int32 | int16 | int8 | uint64 | uint32 | uint16 | uint8 | int | uint | string | float64 | float32
}

//最大值
func Max[T Number](vals []T, xs ...T) (max T) {
	first := true
	if n := len(vals); n > 0 {
		max = vals[0]
		first = false
		for i := 1; i < n; i++ {
			if max < vals[i] {
				max = vals[i]
			}
		}
	}

	if n := len(xs); n > 0 {
		i := 0
		if first {
			max = xs[0]
			i = 1
		}
		for ; i < n; i++ {
			if max < xs[i] {
				max = xs[i]
			}
		}
	}

	return
}

//最小值
func Min[T Number](vals []T, xs ...T) (min T) {
	first := true
	if n := len(vals); n > 0 {
		min = vals[0]
		first = false
		for i := 1; i < n; i++ {
			if min > vals[i] {
				min = vals[i]
			}
		}
	}

	if n := len(xs); n > 0 {
		i := 0
		if first {
			min = xs[0]
			i = 1
		}
		for ; i < n; i++ {
			if min > xs[i] {
				min = xs[i]
			}
		}
	}

	return
}

/**
 * @description:  CROSS(A,B)表示当A从下方向上穿过B时返回1,否则返回0
 * @param {[]float64} a
 * @param {[]float64} b
 * @param {int} day
 * @return {*}
 */
func Cross[T Number](a []T, b []T) bool {
	n := len(a)
	if n != len(b) {
		return false
	}
	if n < 2 {
		return false
	}
	n--
	if a[n] > b[n] && a[n-1] < b[n-1] {
		return true
	}
	return false
}
