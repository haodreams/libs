/*
 * @Author: Wangjun
 * @Date: 2023-03-21 14:35:48
 * @LastEditTime: 2024-12-06 09:35:10
 * @LastEditors: wangjun haodreams@163.com
 * @Description:
 * @FilePath: \libs\easy\if.go
 * hnxr
 */
package easy

//IF = b?x:y
func IF[G any](b bool, x, y G) G {
	if b {
		return x
	}
	return y
}

//判断值是否在给定的列表中
func In[G comparable](x G, args ...G) bool {
	for _, arg := range args {
		if arg == x {
			return true
		}
	}
	return false
}
