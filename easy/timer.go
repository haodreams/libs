/*
 * @Author: Wangjun
 * @Date: 2021-05-15 22:43:19
 * @LastEditTime: 2024-06-20 13:44:27
 * @LastEditors: wangjun haodreams@163.com
 * @Description:
 * @FilePath: \oneareaprod:\go\src\gitee.com\haodreams\libs\easy\timer.go
 * hnxr
 */
package easy

import (
	"time"
)

// Sleep 精确睡眠，当前时间到t时间范围 大于 指定的时间，不睡眠，否则睡眠剩下的时间
func Sleep(t *time.Time, ms int64) {
	msec := time.Duration(ms) * time.Millisecond
	s := time.Since(*t)
	if msec > s {
		msec -= s
		time.Sleep(msec)
	}
}

// Timer ...
type Timer struct {
	interval int64
	last     int64
	trim     bool //自动调整到整点
}

// NewTimer 建立一个指定间隔的定时器,启动后立即执行
func NewTimer(seconds int64) *Timer {
	t := new(Timer)
	t.interval = seconds
	t.trim = true
	//	t.Reset()
	return t
}
func (m *Timer) Trim(t bool) *Timer {
	m.trim = t
	return m
}

// Reset 时间戳复位
func (m *Timer) Reset() {
	m.last = time.Now().Unix()
	if m.trim {
		m.last -= m.last % m.interval
	}
}

// 复位到指定时间戳
func (m *Timer) ResetTS(ts int64) {
	m.last = ts
	if m.trim {
		m.last -= m.last % m.interval
	}
}

// 是否到了指定时间
func (m *Timer) OverTS(ts int64) bool {
	if (ts - m.last) >= m.interval {
		m.ResetTS(ts)
		return true
	}
	return false
}

// Over 时间是否到了
func (m *Timer) Over() bool {
	if (time.Now().Unix() - m.last) >= m.interval {
		m.Reset()
		return true
	}
	return false
}

// OverNow 时间是否到了，返回当前时间和结果
func (m *Timer) OverNow() (now int64, b bool) {
	now = time.Now().Unix()
	if (now - m.last) >= m.interval {
		m.Reset()
		now -= now % m.interval
		b = true
		return
	}
	return
}
