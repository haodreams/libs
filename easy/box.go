/*
 * @Author: Wangjun
 * @Date: 2023-08-19 15:52:45
 * @LastEditTime: 2023-08-19 20:18:52
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \libs\easy\box.go
 * hnxr
 */
package easy

import "encoding/json"

type Box[T any] struct {
	data []T
	idx  int
	cap  int
}

func NewBox[T any](max int) *Box[T] {
	b := new(Box[T])
	b.cap = max
	b.data = make([]T, max)
	return b
}

func (m *Box[T]) Push(a T) (b bool) {
	m.data[m.idx] = a
	m.idx++
	return m.idx >= m.cap
}

func (m *Box[T]) Reset() {
	m.idx = 0
}

func (m *Box[T]) ToJSON() ([]byte, error) {
	return json.Marshal(m.data[:m.idx])
}

func (m *Box[T]) Len() int {
	return m.idx
}

func (m *Box[T]) Cap() int {
	return m.cap
}

func (m *Box[T]) IsFull() bool {
	return m.idx >= m.cap
}

func (m *Box[T]) Get(i int) any {
	if i < m.idx {
		return m.data[i]
	}
	return nil
}

// 获取已存的全部数据
func (m *Box[T]) Data() []T {
	return m.data[:m.idx]
}
