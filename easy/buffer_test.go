/*
 * @Date: 2023-03-18 20:57:41
 * @LastEditors: wangjun haodreams@163.com
 * @LastEditTime: 2024-09-06 14:11:29
 * @FilePath: \libs\ebytes\ebytes.go
 * @Description:byte 的readat 接口实现
 */
package easy

import (
	"log"
	"testing"
)

func TestRead(t *testing.T) {
	r := NewBuffer([]byte("1234567890"))
	buf := make([]byte, 5)
	n, _ := r.Read(buf)
	log.Println(string(buf[:n]))
	n1, _ := r.Seek(3, 1)
	log.Println(n1)
	n, _ = r.Read(buf)
	log.Println(string(buf[:n]))
	n1, _ = r.Seek(3, 0)
	log.Println(n1)
	n, _ = r.Read(buf)
	log.Println(string(buf[:n]))
	n1, _ = r.Seek(-3, 2)
	log.Println(n1)
	n, _ = r.Read(buf)
	log.Println(string(buf[:n]))
	n1, _ = r.Seek(3, 1)
	log.Println(n1)
	n, _ = r.Read(buf)
	log.Println(string(buf[:n]))
}
