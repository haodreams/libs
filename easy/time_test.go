/*
- @Author: Wangjun
- @Date: 2022-01-07 09:36:46

  - @LastEditTime: 2024-06-19 16:30:10

  - @LastEditors: wangjun haodreams@163.com

- @Description:时间测试包
- @FilePath: \libs\easy\time_test.go
- hnxr
*/
package easy

import (
	"encoding/json"
	"log"
	"testing"
	"time"
)

func TestTimeToJson(t *testing.T) {
	tt := new(Time)
	tt.Set(time.Now().Unix())
	b, err := json.Marshal(tt)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println(tt)
	log.Println(string(b))
}

func TestDateToJson(t *testing.T) {
	tt := new(Date)
	tt.Set(time.Now().Unix())
	b, err := json.Marshal(tt)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println(tt)
	log.Println(string(b))
}

func TestParseTime(t *testing.T) {
	s := "2023-04-21 00:00:00"
	t1, _ := ParserLocalTime64(s)
	t2, _ := ParserTime64(s)
	log.Println(t1, t2)
}

func TestMulit(t *testing.T) {
	val := []float64{1, 2, 3, 4}
	Multi(val, 2)
	log.Println(val)
}
