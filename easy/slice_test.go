/*
 * @Author: wangjun haodreams@163.com
 * @Date: 2024-07-07 16:36:17
 * @LastEditors: wangjun haodreams@163.com
 * @LastEditTime: 2024-07-07 16:38:36
 * @FilePath: \libs\easy\lsit_test.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package easy

import (
	"fmt"
	"testing"
)

func TestChunkSlice(t *testing.T) {
	// 创建一个大数组
	largeArray := make([]int, 25)
	for i := range largeArray {
		largeArray[i] = i + 100
	}

	// 将大数组拆分为大小为10的小数组
	smallerArrays := ChunkSlice(largeArray, 10)

	// 打印出所有的小数组
	for _, smallArray := range smallerArrays {
		fmt.Println(smallArray)
	}
}
