/*
- @Author: Wangjun
- @Date: 2022-01-07 09:36:46

  - @LastEditTime: 2024-06-20 13:46:42

  - @LastEditors: wangjun haodreams@163.com

- @Description:时间测试包
- @FilePath: \libs\easy\time_test.go
- hnxr
*/
package easy

import (
	"log"
	"testing"
	"time"
)

func TestTimer(t *testing.T) {
	ts := NewTimer(10) //总召唤间隔定时器

	for {
		now := time.Now().Unix()
		if ts.OverTS(now) {
			log.Println(FormatTime(now))
		}
		time.Sleep(time.Millisecond * 50)
	}
}
