/*
 * @Author: wangjun haodreams@163.com
 * @Date: 2024-08-20 17:28:40
 * @LastEditors: wangjun haodreams@163.com
 * @LastEditTime: 2024-08-22 09:43:55
 * @FilePath: \libs\easy\json.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package easy

import (
	"encoding/json"
	"os"
)

func LoadJSON(path string, v any) (err error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return
	}
	err = json.Unmarshal(data, v)
	return
}

func SaveJSON(path string, v any) (err error) {
	data, err := json.Marshal(v)
	if err != nil {
		return
	}
	err = os.WriteFile(path, data, 0644)

	return
}
