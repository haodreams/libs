/*
 * @Author: wangjun haodreams@163.com
 * @Date: 2024-07-07 16:34:51
 * @LastEditors: wangjun haodreams@163.com
 * @LastEditTime: 2024-07-07 16:35:41
 * @FilePath: \libs\easy\list.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package easy

//一个大数组拆分为多个小数组
func ChunkSlice[T any](slice []T, size int) [][]T {
	var chunks [][]T
	for i := 0; i < len(slice); i += size {
		end := i + size
		if end > len(slice) {
			end = len(slice)
		}
		chunks = append(chunks, slice[i:end])
	}
	return chunks
}
