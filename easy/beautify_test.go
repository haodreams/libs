/*
 * @Author: Wangjun
 * @Date: 2022-11-15 09:22:19
 * @LastEditTime: 2022-11-15 09:29:50
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \libs\easy\beautify_test.go
 * hnxr
 */
package easy

import (
	"log"
	"testing"
)

func TestParse(t *testing.T) {
	value, err := ParseSize("10 GB")
	log.Println(value, err, BeautifySize(value))
	value, err = ParseSize("10 G")
	log.Println(value, err, BeautifySize(value))
	value, err = ParseSize("10G")
	log.Println(value, err, BeautifySize(value))
	value, err = ParseSize("10GB")
	log.Println(value, err, BeautifySize(value))
	value, err = ParseSize("10 B")
	log.Println(value, err, BeautifySize(value))
	value, err = ParseSize("10 ")
	log.Println(value, err, BeautifySize(value))
	value, err = ParseSize("10 K")
	log.Println(value, err, BeautifySize(value))
	value, err = ParseSize("10KB")
	log.Println(value, err, BeautifySize(value))
}
