/*
 * @Author: Wangjun
 * @Date: 2021-12-14 17:06:59
 * @LastEditTime: 2024-12-28 12:42:48
 * @LastEditors: wangjun haodreams@163.com
 * @Description:
 * @FilePath: \libs\easy\string_test.go
 * hnxr
 */
package easy

import (
	"log"
	"reflect"
	"testing"
)

type age1 struct {
	Age1 int
}

type Pos1 struct {
	Name string
	ID   int
	Desc *int
	age1
	data int
}

func TestNameID2s(t *testing.T) {
	var ps []*Pos1
	var names []string
	var paths [][]int
	NameID(reflect.TypeOf(ps), &names, &paths, nil)
	for i := range names {
		log.Println(names[i], paths[i])
	}
}

func TestNameIDs(t *testing.T) {
	var names []string
	var paths [][]int
	p := &Pos1{data: 0}
	NameID(reflect.TypeOf(p), &names, &paths, nil)

	for i := range names {
		log.Println(names[i], paths[i])
	}
	v := reflect.ValueOf(p)
	v = reflect.Indirect(v)
	field := v.FieldByIndex(paths[1])
	field.SetInt(100)
	log.Println(p.ID)
	field = v.FieldByIndex(paths[2])
	if field.Type().Kind() == reflect.Ptr {
		if field.IsNil() {
			field.Set(reflect.New(field.Type().Elem()))
		}
		field = field.Elem()
	}
	field.SetInt(200)
	log.Println(*p.Desc)
}
