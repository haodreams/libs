/*
 * @Author: Wangjun
 * @Date: 2021-12-14 17:06:59
 * @LastEditTime: 2024-12-28 17:02:09
 * @LastEditors: wangjun haodreams@163.com
 * @Description:
 * @FilePath: \libs\easy\string_test.go
 * hnxr
 */
package easy

import (
	"testing"
)

type age struct {
	Age1 int
}

type Pos struct {
	Name string
	ID   int
	Desc *int //*int FieldByIndex 暂不支持，有需要时再支持
	age
	//data int
}

func TestStructTitles(t *testing.T) {
	// var pos Pos
	// pos.data = 100
	// pos.Name = "abc"
	// pos.Age1 = 18
	// pos.Desc = &pos.data
	// var pos1 []Pos
	// var pos2 []*Pos

	// aa := make([]int, 10)

	// titles := StructTitles(pos)
	// log.Println(titles)
	// titles = StructTitles(pos1)
	// log.Println(titles)
	// titles = StructTitles(pos2)
	// log.Println(titles)
	// titles = StructFields(pos)
	// log.Println(titles)
	// titles = StructFields(pos)
	// log.Println(titles)
	// titles = StructTitles(aa)
	// log.Println(titles)
	// titles = StructFields(aa)
	// log.Println(titles)

}
