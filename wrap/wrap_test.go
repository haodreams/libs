package wrap

import (
	"log"
	"testing"
)

// TestOKWarp 正确用法
func TestOKWarp(t *testing.T) {
	wg := NewWaitGroupWrapper(0)
	for i := 0; i < 100; i++ {
		ii := i
		wg.Wrap(func() {
			log.Println(ii)
		})
	}
	wg.Wait()
}

// TestOKWarp 正确用法
func TestChanWarp(t *testing.T) {
	ress := make(chan int, 100)
	go func() {
		for i := 0; i < 100; i++ {
			ress <- i
		}
		close(ress)
	}()
	wg := NewWaitGroupWrapper(0)
	for i := 0; i < 5; i++ {
		idx := i
		wg.Wrap(func() {
			for res := range ress {
				log.Println(idx+1, res)
			}
		})
	}
	wg.Wait()
}

// TestErrorWarp 错误用法
func TestErrorWarp(t *testing.T) {
	wg := NewWaitGroupWrapper(0)
	for i := 0; i < 100; i++ {
		wg.Wrap(func() {
			log.Println(i)
		})
	}
	wg.Wait()
}
