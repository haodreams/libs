/*
 * @Author: Wangjun
 * @Date: 2021-05-15 22:43:20
 * @LastEditTime: 2023-10-25 16:54:02
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \xr_node_calcd:\go\src\gitee.com\haodreams\libs\wrap\wrap.go
 * hnxr
 */
package wrap

import (
	"sync"
	"sync/atomic"
	"time"
)

// WaitGroupWrap 支持设置最大线程数的wg
func WaitGroupWrap[G any](maxThread int, chanAny chan *G, cb func(g *G)) *sync.WaitGroup {
	wg := new(sync.WaitGroup)
	wg.Add(maxThread)
	for i := 0; i < maxThread; i++ {
		go func() {
			for g := range chanAny {
				cb(g)
			}
			wg.Done()
		}()
	}
	return wg
}

// Deprecated WaitGroupWrapper 建议使用WaitGroupWrap
type WaitGroupWrapper struct {
	sync.WaitGroup
	num int32
	max int32 //最大线程数
}

// Deprecated NewWaitGroupWrapper 支持设置最大线程数的wg
func NewWaitGroupWrapper(maxThread int) *WaitGroupWrapper {
	wg := new(WaitGroupWrapper)
	wg.max = int32(maxThread)
	return wg
}

// Wrap cb 回调函数
// 回调函数需要注意的是 回调函数内部的变量是引用变量，需要注意创建新变量复制
func (m *WaitGroupWrapper) Wrap(cb func()) {
	if m.max > 0 {
		atomic.AddInt32(&m.num, 1)
	}

	m.Add(1)
	go func() {
		defer m.Done()
		defer func() {
			if m.max > 0 {
				atomic.AddInt32(&m.num, -1)
			}
		}()
		cb()
	}()

	if m.max > 0 {
		for atomic.LoadInt32(&m.num) > m.max {
			time.Sleep(time.Millisecond * 50)
		}
	}
}
