/*
 * @Author: wangjun haodreams@163.com
 * @Date: 2024-07-25 15:43:40
 * @LastEditors: wangjun haodreams@163.com
 * @LastEditTime: 2024-07-26 15:44:55
 * @FilePath: \libs\axis\span_test.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package span

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"testing"

	"gitee.com/haodreams/libs/easy"
)

func loadTestData(path string) (tps []*TimePoint, err error) {
	lines, err := easy.ReadLines(path)
	if err != nil {
		log.Println(err)
		return
	}

	for _, line := range lines {
		ss := strings.Split(line, ",")
		if len(ss) == 2 {
			t, err := easy.ParserLocalTime64(ss[0])
			if err != nil {
				continue
			}
			fval, err := strconv.ParseFloat(ss[1], 64)
			if err != nil {
				continue
			}
			tps = append(tps, &TimePoint{X: t, Y: fval})
			//rawspan.Put(t, fval)
		}
	}
	return
}

func TestSpan(t *testing.T) {

	begin, err := easy.ParserLocalTime64("2024-07-25 05:00:00")
	if err != nil {
		log.Println(err)
		return
	}
	end, err := easy.ParserLocalTime64("2024-07-25 16:00:00")
	if err != nil {
		log.Println(err)
		return
	}

	interval := (end - begin) / 250
	rawspan := NewRawSpan(begin, end, interval)
	tps, err := loadTestData("gri.csv")
	if err != nil {
		log.Println(err)
		return
	}
	for _, tp := range tps {
		rawspan.Put(tp.X, tp.Y)
	}

	buf := bytes.NewBuffer(nil)
	rawspan.Update()
	for _, point := range rawspan.fvals {
		buf.WriteString(fmt.Sprintf("%.2f\n", point))
	}
	os.WriteFile("temp.csv", buf.Bytes(), 0644)
}
