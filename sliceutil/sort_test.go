/*
 * @Author: Wangjun
 * @Date: 2021-05-15 22:43:20
 * @LastEditTime: 2022-05-20 15:02:15
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \libs\sliceutil\sort_test.go
 * hnxr
 */
package sliceutil

import (
	"fmt"
	"log"
	"testing"
)

type tt struct {
	Name string
	ID   int
}

// TestSort .
func TestSort(t *testing.T) {
	tts := make([]*tt, 10)
	for i := range tts {
		tts[i] = new(tt)
		tts[i].Name = fmt.Sprint(100 - i)
		tts[i].ID = 20 - i
	}
	for i := range tts {
		log.Printf("%+v", tts[i])
	}
	log.Println("======================")
	err := Asc(&tts, "ID")
	if err != nil {
		log.Println(err)
	}
	for i := range tts {
		log.Printf("%+v", tts[i])
	}
}

func TestWhere(t *testing.T) {
	tts := make([]*tt, 10)
	for i := range tts {
		tts[i] = new(tt)
		tts[i].Name = fmt.Sprint(100 - i)
		tts[i].ID = 20 - i
	}
	// for i := range tts {
	// 	log.Printf("%+v", tts[i])
	// }
	log.Println("======================")
	aab := WhereMap(map[string]string{"search_ID": "19"}, tts, false)
	// if err != nil {
	// 	log.Println(err)
	// }

	if aa, ok := aab.([]*tt); ok {
		//log.Printf("%+v", aa)
		//DescByField(tts, "Name")
		for i := range aa {
			log.Printf("%+v", aa[i])
		}
	}

}
