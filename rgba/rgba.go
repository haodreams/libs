/*
 * @Author: wangjun haodreams@163.com
 * @Date: 2024-07-24 22:33:19
 * @LastEditors: wangjun haodreams@163.com
 * @LastEditTime: 2024-07-27 14:10:05
 * @FilePath: \libs\rgba\rgba.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package rgba

import (
	"image"
	"image/color"
)

func pixOffset(x, y, stride int) int {
	return y*stride + x*4
}

func rect(img *image.RGBA, x, y, width, height int, fill color.Color) {
	r, g, b, a := fill.RGBA()
	rgb := []uint8{uint8(r), uint8(g), uint8(b), uint8(a)}
	idx := pixOffset(x, y, img.Stride)
	begin := idx
	//先画出首行的填充
	for x1 := x; x1 < x+width; x1++ {
		if x1 > img.Rect.Dx() {
			break
		}
		copy(img.Pix[idx:idx+4:idx+4], rgb)
		idx += 4
	}
	end := idx
	w := width * 4
	//复制首行的填充
	for y1 := y + 1; y1 < y+height; y1++ {
		idx = pixOffset(x, y1, img.Stride)
		if idx+w > len(img.Pix) {
			return
		}
		copy(img.Pix[idx:], img.Pix[begin:end])
	}
}

// 高效画出矩形
func Rect(img *image.RGBA, x, y, width, height int, fill color.Color, border color.Color, borderWidth int) {
	if x < 0 || y < 0 || width <= 0 || height <= 0 {
		return
	}
	if fill == nil || border == nil {
		if fill != nil {
			rect(img, x, y, width, height, fill)
		}
		if border != nil {
			rect(img, x, y, width, height, border)
			return
		}
		return
	}

	if borderWidth == 0 || width <= borderWidth*2 || height <= borderWidth*2 {
		rect(img, x, y, width, height, border)
		return
	}
	w := width * 4

	r, g, b, a := fill.RGBA()
	rgbFill := []uint8{uint8(r), uint8(g), uint8(b), uint8(a)}
	r, g, b, a = border.RGBA()
	rgbBorder := []uint8{uint8(r), uint8(g), uint8(b), uint8(a)}
	idx := pixOffset(x, y, img.Stride)
	begin := idx
	//先画出首行的填充
	for x1 := x; x1 < x+width; x1++ {
		if x1 > img.Rect.Dx() {
			break
		}
		copy(img.Pix[idx:], rgbBorder)
		idx += 4
	}
	end := idx

	//复制首行的填充到边框宽度
	for y1 := y + 1; y1 < y+borderWidth; y1++ {
		idx = pixOffset(x, y1, img.Stride)
		copy(img.Pix[idx:], img.Pix[begin:end])
	}
	//复制首行的填充到底部边框
	h := y + height - 1 - borderWidth
	for y1 := y + height - 1; y1 >= h; y1-- {
		idx = pixOffset(x, y1, img.Stride)
		copy(img.Pix[idx:], img.Pix[begin:end])
	}

	//先画出次首行的填充
	idx = pixOffset(x, y+borderWidth, img.Stride)
	copy(img.Pix[idx:], img.Pix[begin:end])
	begin = pixOffset(x, y+borderWidth, img.Stride)
	end = begin + width*4
	idx = begin + (borderWidth * 4)
	for x1 := x + borderWidth; x1 < x+width-borderWidth; x1++ {
		if x1 > img.Rect.Dx() {
			break
		}
		copy(img.Pix[idx:], rgbFill)
		idx += 4
	}
	for y1 := y + borderWidth; y1 < y+height-borderWidth; y1++ {
		idx = pixOffset(x, y1, img.Stride)
		if idx+w > len(img.Pix) {
			return
		}
		copy(img.Pix[idx:], img.Pix[begin:end])
	}
}

func CopyRGBA(dst *image.RGBA, src *image.RGBA) {
	copy(dst.Pix, src.Pix)
}

var (
	// Red is the basic theme red color.
	Red = color.RGBA{R: 255, G: 0, B: 0, A: 255}
	// Yellow is the basic theme yellow color.
	Yellow = color.RGBA{R: 217, G: 210, B: 0, A: 255}
	// Green is the basic theme green color.
	Green = color.RGBA{R: 0, G: 217, B: 101, A: 255}
	// Blue is the basic theme blue color.
	Blue = color.RGBA{R: 0, G: 116, B: 217, A: 255}
	// White is white.
	White = color.RGBA{R: 255, G: 255, B: 255, A: 255}
	// Cyan is the basic theme cyan color.
	Cyan = color.RGBA{R: 0, G: 217, B: 210, A: 255}
	// Orange is the basic theme orange color.
	Orange = color.RGBA{R: 217, G: 101, B: 0, A: 255}
	// Black is the basic theme black color.
	Black = color.RGBA{R: 0, G: 0, B: 0, A: 255}
	// LightGray is the basic theme light gray color.
	LightGray = color.RGBA{R: 239, G: 239, B: 239, A: 255}

	// AlternateBlue is a alternate theme color.
	AlternateBlue = color.RGBA{R: 106, G: 195, B: 203, A: 255}
	// AlternateGreen is a alternate theme color.
	AlternateGreen = color.RGBA{R: 42, G: 190, B: 137, A: 255}
	// AlternateGray is a alternate theme color.
	AlternateGray = color.RGBA{R: 110, G: 128, B: 139, A: 255}
	// AlternateYellow is a alternate theme color.
	AlternateYellow = color.RGBA{R: 240, G: 174, B: 90, A: 255}
	// AlternateLightGray is a alternate theme color.
	AlternateLightGray = color.RGBA{R: 187, G: 190, B: 191, A: 255}
)
