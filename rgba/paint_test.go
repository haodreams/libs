/*
 * @Author: wangjun haodreams@163.com
 * @Date: 2024-07-24 20:32:27
 * @LastEditors: wangjun haodreams@163.com
 * @LastEditTime: 2024-07-24 22:34:22
 * @FilePath: \dataview\paint_test.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package rgba

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"os"
	"testing"
)

func TestGC3(t *testing.T) {
	img := image.NewRGBA(image.Rect(0, 0, 1000, 1000))
	Rect(img, 100, 100, 100, 100, color.RGBA{R: 0, G: 0, B: 0, A: 255}, color.RGBA{R: 255, G: 255, B: 255, A: 255}, 2)
	dstFile, err := os.Create("ac.png")
	if err != nil {
		fmt.Println("Error creating destination file:", err)
		return
	}
	defer dstFile.Close()

	err = png.Encode(dstFile, img)
	if err != nil {
		fmt.Println("Error encoding image:", err)
		return
	}
}

func TestGC(t *testing.T) {
	img := image.NewRGBA(image.Rect(0, 0, 1000, 1000))
	rect(img, 100, 100, 100, 100, color.RGBA{R: 0, G: 0, B: 0, A: 255})
	dstFile, err := os.Create("aa.png")
	if err != nil {
		fmt.Println("Error creating destination file:", err)
		return
	}
	defer dstFile.Close()

	err = png.Encode(dstFile, img)
	if err != nil {
		fmt.Println("Error encoding image:", err)
		return
	}
}

func BenchmarkGC2(b *testing.B) {
	img := image.NewRGBA(image.Rect(0, 0, 1000, 1000))
	for i := 0; i < b.N; i++ {
		Rect(img, 0, 0, 1000, 1000, color.RGBA{R: 0, G: 0, B: 0, A: 255}, color.RGBA{R: 255, G: 255, B: 255, A: 255}, 0)
	}
}
