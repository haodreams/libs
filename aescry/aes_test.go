/*
 * @Author: Wangjun
 * @Date: 2022-07-13 13:46:01
 * @LastEditTime: 2022-07-13 13:48:32
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \libs\aescry\aes_test.go
 * hnxr
 */
package aescry

import (
	"encoding/base64"
	"fmt"
	"testing"
)

func TestAes(t *testing.T) {
	var aeskey = []byte(MakeKey("321423u9y8d2fwfl"))
	pass := []byte("test123454556666890")
	xpass, err := Encrypt(pass, aeskey)
	if err != nil {
		fmt.Println(err)
		return
	}
	pass64 := base64.StdEncoding.EncodeToString(xpass)
	fmt.Printf("length:%d,加密后:%v\n", len(xpass), pass64)
	bytesPass, err := base64.StdEncoding.DecodeString(pass64)
	if err != nil {
		fmt.Println(err)
		return
	}
	tpass, err := Decrypt(bytesPass, aeskey)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("解密后:%s\n", tpass)
}
