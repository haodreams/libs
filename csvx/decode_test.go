/*
 * @Author: Wangjun
 * @Date: 2022-10-20 14:01:16
 * @LastEditTime: 2024-12-28 16:50:25
 * @LastEditors: wangjun haodreams@163.com
 * @Description:
 * @FilePath: \golib\csvstruct\decode_test.go
 * hnxr
 */
package csvx

import (
	"encoding/json"
	"log"
	"testing"
)

type tc struct {
	Age int
}

type Test struct {
	ID   int
	Desc *string
	Name string
	tc
}

var data = `ID,desc,Name,Age
1,,"a1,cc",100
2,11,b1,10
3,,b3,96`

var dataJson = `[
{"ID":1,"Desc":null,"Name:"a1","Age",100},
{"ID":2,"Desc":"11","Name:"b1","Age",100},
{"ID":3,"Desc":null,"Name:"c1","Age",100},
]`

func TestUnmarshal(t *testing.T) {
	var k []*Test
	err := Unmarshal([]byte(data), &k)
	if err != nil {
		log.Println(err)
		return
	}
	for _, b := range k {
		log.Printf("%+v", *b)
	}
	d, err := Marshal(k)
	if err != nil {
		log.Println(err)
		return
	}
	log.Print(string(d))
}

func BenchmarkUnCsv(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		var k []*Test
		Unmarshal([]byte(data), &k)
	}
}

func BenchmarkUnJson(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		var k []*Test
		json.Unmarshal([]byte(dataJson), &k)
	}
}

func BenchmarkCsv(b *testing.B) {
	var k []*Test
	k = append(k, &Test{ID: 1, Name: "bb,cc"})
	k = append(k, &Test{ID: 2})
	k = append(k, &Test{ID: 3})
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		Marshal(k)
	}
}

// func BenchmarkCsv2(b *testing.B) {
// 	var k []*Test
// 	k = append(k, &Test{ID: 1, Name: "bb,cc"})
// 	k = append(k, &Test{ID: 2})
// 	k = append(k, &Test{ID: 3})
// 	b.ResetTimer()
// 	for i := 0; i < b.N; i++ {
// 		Encode2(k)
// 	}
// }

func BenchmarkJson(b *testing.B) {
	var k []*Test
	k = append(k, &Test{ID: 1})
	k = append(k, &Test{ID: 2})
	k = append(k, &Test{ID: 3})
	for i := 0; i < b.N; i++ {
		json.Marshal(k)
	}
}
