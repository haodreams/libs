package mock

import (
	"log"
	"testing"
)

func TestMock(t *testing.T) {
	m := NewMock(10, 15)
	for i := 0; i < 100; i++ {
		log.Println(m.Value())
	}
}
