/*
 * @Author: wangjun haodreams@163.com
 * @Date: 2024-08-08 10:24:50
 * @LastEditors: wangjun haodreams@163.com
 * @LastEditTime: 2024-08-08 15:15:43
 * @FilePath: \libs\mock\mock.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package mock

import (
	"math"
	"math/rand"
	"time"
)

var rd = rand.New(rand.NewSource(time.Now().UnixNano()))

func Rand(max int64) int64 {
	return rd.Int63n(max)
}

type Mock struct {
	HighLimit float64 //上限
	LowLimit  float64 //下限
	value     float64
	avg       float64
	isUp      bool
}

func NewMock(lowHigh, highLimit float64) *Mock {
	if lowHigh > highLimit {
		lowHigh, highLimit = highLimit, lowHigh

	}
	m := &Mock{
		HighLimit: highLimit,
		LowLimit:  lowHigh,
	}
	m.avg = (highLimit-lowHigh)/10 + 0.0001
	m.value = (highLimit-lowHigh)/2 + lowHigh
	return m
}

func (m *Mock) SetValue(value float64) *Mock {
	m.value = value
	return m
}

func (m *Mock) Value() (value float64) {
	randValue := rd.Int63n(10000)
	value = float64(randValue%int64(m.avg*100+1)) / 200
	if m.isUp {
		m.value += value
		if m.value > m.HighLimit {
			//66% 的几率改变方向
			if randValue < 6600 {
				m.isUp = false
			}
		} else {
			//25% 的几率改变方向
			if randValue < 2500 {
				m.isUp = false
			}
		}
	} else {
		m.value -= value
		if m.value < m.LowLimit {
			//66% 的几率改变方向
			if randValue < 6600 {
				m.isUp = true
			}
		} else {
			//25% 的几率改变方向
			if randValue < 2500 {
				m.isUp = true
			}
		}
	}

	//在中线附近有30%的几率改变方向
	if math.Abs((m.LowLimit+m.avg*5)-m.value) < m.avg/2 {
		//fmt.Println("30%")
		//30% 的几率改变方向
		if randValue < 3000 {
			m.isUp = !m.isUp
		}
	}
	return m.value
}
