/*
 * @Author: Wangjun
 * @Date: 2023-03-20 18:06:06
 * @LastEditTime: 2024-02-19 20:41:28
 * @LastEditors: wangjun haodreams@163.com
 * @Description:
 * @FilePath: \aexp\op.go
 * hnxr
 */
package aexp

import (
	"errors"
	"go/token"
)

type OPer interface {
	Eval(x, y any) (any, error)
	String() string
}

// type FuncOP func(x, y any) (any, error)
func eval(x, y any, msg string, f func(x1, y1 float64) (float64, error)) (any, error) {
	if x11, ok := x.(float64); ok {
		if y11, ok := y.(float64); ok {
			return f(x11, y11)
		}
	}
	return nil, errors.New(msg + "参数错误")
}

type add struct {
}

func (m *add) String() string {
	return "+"
}

func (m *add) Eval(x, y any) (any, error) {
	return eval(x, y, "'+':", func(x1 float64, y1 float64) (float64, error) {
		return x1 + y1, nil
	})
}

type sub struct {
}

func (m *sub) String() string {
	return "-"
}

func (m *sub) Eval(x, y any) (any, error) {
	return eval(x, y, "'-':", func(x1 float64, y1 float64) (float64, error) {
		return x1 - y1, nil
	})
}

type mul struct {
}

func (m *mul) String() string {
	return "*"
}

func (m *mul) Eval(x, y any) (any, error) {
	return eval(x, y, "'*':", func(x1 float64, y1 float64) (float64, error) {
		return x1 * y1, nil
	})
}

type quo struct {
}

func (m *quo) String() string {
	return "/"
}

func (m *quo) Eval(x, y any) (any, error) {
	return eval(x, y, "'/':", func(x1 float64, y1 float64) (float64, error) {
		if y1 == 0 {
			return 0, errors.New("division by zero")
		}
		return x1 / y1, nil
	})
}

type rem struct {
}

func (m *rem) String() string {
	return "%"
}

func (m *rem) Eval(x, y any) (any, error) {
	return eval(x, y, "'%':", func(x1 float64, y1 float64) (float64, error) {
		if y1 == 0 {
			return 0, errors.New("% by zero")
		}
		return float64(int64(x1) % int64(y1)), nil
	})
}

type and struct {
}

func (m *and) String() string {
	return "&"
}

func (m *and) Eval(x, y any) (any, error) {
	return eval(x, y, "'&':", func(x1 float64, y1 float64) (float64, error) {
		return float64(int64(x1) & int64(y1)), nil
	})
}

type or struct {
}

func (m *or) String() string {
	return "|"
}

func (m *or) Eval(x, y any) (any, error) {
	return eval(x, y, "'|':", func(x1 float64, y1 float64) (float64, error) {
		return float64(int64(x1) | int64(y1)), nil
	})
}

type land struct {
}

func (m *land) String() string {
	return "&&"
}

func (m *land) Eval(x, y any) (any, error) {
	return eval(x, y, "'&&':", func(x1 float64, y1 float64) (float64, error) {
		if (int64(x1) != 0) && (int64(y1) != 0) {
			return 1.0, nil
		}
		return 0.0, nil
	})
}

type lor struct {
}

func (m *lor) String() string {
	return "||"
}

func (m *lor) Eval(x, y any) (any, error) {
	return eval(x, y, "'||':", func(x1 float64, y1 float64) (float64, error) {
		if (int64(x1) != 0) || (int64(y1) != 0) {
			return 1.0, nil
		}
		return 0.0, nil
	})
}

type xor struct {
}

func (m *xor) String() string {
	return "^"
}

func (m *xor) Eval(x, y any) (any, error) {
	return eval(x, y, "'^':", func(x1 float64, y1 float64) (float64, error) {
		return float64(int64(x1) ^ int64(y1)), nil
	})
}

type shl struct {
}

func (m *shl) String() string {
	return "<<"
}

func (m *shl) Eval(x, y any) (any, error) {
	return eval(x, y, "'<<':", func(x1 float64, y1 float64) (float64, error) {
		return float64(int64(x1) << int64(y1)), nil
	})
}

type shr struct {
}

func (m *shr) String() string {
	return ">>"
}

func (m *shr) Eval(x, y any) (any, error) {
	return eval(x, y, "'>>':", func(x1 float64, y1 float64) (float64, error) {
		return float64(int64(x1) >> int64(y1)), nil
	})
}

type eql struct {
}

func (m *eql) String() string {
	return "=="
}

func (m *eql) Eval(x, y any) (any, error) {
	switch x1 := x.(type) {
	case float64:
		if y1, ok := y.(float64); ok {
			if x1 == y1 {
				return 1.0, nil
			}
			return 0.0, nil
		}
	case string:
		if y1, ok := y.(string); ok {
			if x1 == y1 {
				return 1.0, nil
			}
			return 0.0, nil
		}
	}
	return 0, errors.New("'=='参数错误")
}

type gtr struct {
}

func (m *gtr) String() string {
	return ">"
}

func (m *gtr) Eval(x, y any) (any, error) {
	switch x1 := x.(type) {
	case float64:
		if y1, ok := y.(float64); ok {
			if x1 > y1 {
				return 1.0, nil
			}
			return 0.0, nil
		}
	case string:
		if y1, ok := y.(string); ok {
			if x1 > y1 {
				return 1.0, nil
			}
			return 0.0, nil
		}
	}
	return 0, errors.New("'>'参数错误")
}

type lss struct {
}

func (m *lss) String() string {
	return "<"
}

func (m *lss) Eval(x, y any) (any, error) {
	switch x1 := x.(type) {
	case float64:
		if y1, ok := y.(float64); ok {
			if x1 < y1 {
				return 1.0, nil
			}
			return 0.0, nil
		}
	case string:
		if y1, ok := y.(string); ok {
			if x1 < y1 {
				return 1.0, nil
			}
			return 0.0, nil
		}
	}
	return 0, errors.New("'<'参数错误")
}

type neq struct {
}

func (m *neq) String() string {
	return "!="
}

func (m *neq) Eval(x, y any) (any, error) {
	switch x1 := x.(type) {
	case float64:
		if y1, ok := y.(float64); ok {
			if x1 != y1 {
				return 1.0, nil
			}
			return 0.0, nil
		}
	case string:
		if y1, ok := y.(string); ok {
			if x1 != y1 {
				return 1.0, nil
			}
			return 0.0, nil
		}
	}
	return 0, errors.New("'!='参数错误")
}

type leq struct {
}

func (m *leq) String() string {
	return "<="
}

func (m *leq) Eval(x, y any) (any, error) {
	switch x1 := x.(type) {
	case float64:
		if y1, ok := y.(float64); ok {
			if x1 <= y1 {
				return 1.0, nil
			}
			return 0.0, nil
		}
	case string:
		if y1, ok := y.(string); ok {
			if x1 <= y1 {
				return 1.0, nil
			}
			return 0.0, nil
		}
	}
	return 0, errors.New("'<='参数错误")
}

type geq struct {
}

func (m *geq) String() string {
	return ">="
}

func (m *geq) Eval(x, y any) (any, error) {
	switch x1 := x.(type) {
	case float64:
		if y1, ok := y.(float64); ok {
			if x1 >= y1 {
				return 1.0, nil
			}
			return 0.0, nil
		}
	case string:
		if y1, ok := y.(string); ok {
			if x1 >= y1 {
				return 1.0, nil
			}
			return 0.0, nil
		}
	}
	return 0, errors.New("'>='参数错误")
}

var not = new(unaryNot) //非
var neg = new(unarySub) //负数操作符

var OP = map[token.Token]OPer{
	token.ADD:  new(add),
	token.SUB:  new(sub),
	token.MUL:  new(mul),
	token.QUO:  new(quo),
	token.REM:  new(rem),
	token.AND:  new(and),
	token.OR:   new(or),
	token.XOR:  new(xor),
	token.SHL:  new(shl),
	token.SHR:  new(shr),
	token.LAND: new(land),
	token.LOR:  new(lor),
	token.GTR:  new(gtr),
	token.LSS:  new(lss),
	token.NEQ:  new(neq),
	token.EQL:  new(eql),
	token.GEQ:  new(geq),
	token.LEQ:  new(leq),
}

type Evaler interface {
	Eval() (any, error)
	String() string
}
