/*
 * @Author: Wangjun
 * @Date: 2023-04-13 11:11:47
 * @LastEditTime: 2024-03-09 11:01:56
 * @LastEditors: wangjun haodreams@163.com
 * @Description:
 * @FilePath: \aexp\function.go
 * hnxr
 */
package aexp

import (
	"errors"
	"math"
)

type Function func(x ...any) (any, error)

// 全局函数
var globalFunction = map[string]Function{
	"MIN": MIN,
	"MAX": MAX,
	"ABS": ABS,
	"NOT": NOT,
}

/**
 * @description: 是否是内置函数
 * @param {string} name
 * @return {*}
 */
func IsBuiltinFunction(name string) bool {
	_, ok := globalFunction[name]
	return ok
}

// 注册一个函数
// 使用 包名直接注册的是全局函数，使用Expr 注册的只在Expr内部可用，是私有方法
func RegisterFunction(name string, f Function) {
	globalFunction[name] = f
}

func MAX(xs ...any) (any, error) {
	if len(xs) < 2 {
		return nil, errors.New("[MAX]参数错误")
	}
	return eval(xs[0], xs[1], "", func(x1 float64, y1 float64) (float64, error) {
		if x1 < y1 {
			return y1, nil
		}
		return x1, nil
	})
}

func MIN(xs ...any) (any, error) {
	if len(xs) < 2 {
		return nil, errors.New("[MIN]参数错误")
	}
	return eval(xs[0], xs[1], "", func(x1 float64, y1 float64) (float64, error) {
		if x1 > y1 {
			return y1, nil
		}
		return x1, nil
	})
}

func ABS(xs ...any) (any, error) {
	if len(xs) < 1 {
		return nil, errors.New("'ABS'参数错误")
	}
	if p, ok := xs[0].(float64); ok {
		return math.Abs(p), nil
	}
	return nil, errors.New("'ABS'参数错误")
}

func NOT(xs ...any) (any, error) {
	if len(xs) < 1 {
		return nil, errors.New("'NOT'参数错误")
	}
	if p, ok := xs[0].(float64); ok {
		if int(p) != 0 {
			return 1, nil
		}
		return 0, nil
	}

	return nil, errors.New("'NOT'参数错误")
}
