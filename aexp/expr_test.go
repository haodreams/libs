/*
 * @Author: Wangjun
 * @Date: 2023-03-22 09:31:39
 * @LastEditTime: 2024-06-05 09:52:21
 * @LastEditors: wangjun haodreams@163.com
 * @Description:
 * @FilePath: \aexp\expr_test.go
 * hnxr
 */
package aexp

import (
	"log"
	"testing"
)

func TestEQ(t *testing.T) {
	m := map[string]any{
		"a1.a2": "10",
		"a3.a4": "5",
	}
	e := New(NewMethod(m))

	err := e.Parse(`a1.a2=="10"`)
	if err != nil {
		log.Fatal(err)
	}
	v, err := e.Eval()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(v)

	err = e.Parse(`a1.a2== a3.a4`)
	if err != nil {
		log.Fatal(err)
	}

	log.Println(e.Vars())
	v, err = e.Eval()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(v)

	err = e.Parse(`a1.a2!= a3.a4`)
	if err != nil {
		log.Fatal(err)
	}

	log.Println(e.Vars())
	v, err = e.Eval()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(v)
}
