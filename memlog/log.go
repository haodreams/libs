package memlog

import (
	"strings"

	"gitee.com/haodreams/libs/easy"
	"gitee.com/haodreams/libs/fixedlist"
)

type MemoryLog struct {
	l *fixedlist.FixList[string]
}

func New(max int) *MemoryLog {
	ml := new(MemoryLog)
	ml.Setup(max)
	return ml
}

func (m *MemoryLog) Setup(max int) {
	if max < 10 {
		max = 10
	}
	if max > 500000 {
		max = 500000
	}

	m.l = fixedlist.NewFixList[string](max)
}

// 实现io.Writer 接口
func (m *MemoryLog) Write(b []byte) (n int, err error) {
	m.write(string(b))
	n = len(b)
	return
}

func (m *MemoryLog) write(msg string) {
	m.l.Push(&msg)
}

func (m *MemoryLog) Log(msg string) {
	msg = easy.Now() + " " + msg
	m.write(msg)
}

func (m *MemoryLog) Info(msg string) {
	msg = easy.Now() + " [I] " + msg
	m.write(msg)
}

func (m *MemoryLog) Warn(msg string) {
	msg = easy.Now() + " [W] " + msg
	m.write(msg)
}

func (m *MemoryLog) Error(msg string) {
	msg = easy.Now() + " [E] " + msg
	m.write(msg)
}

func (m *MemoryLog) Page(begin, limit int, orderAsc ...bool) string {
	var s strings.Builder
	ss := m.l.Stack(begin, limit, orderAsc...)
	for i := range ss {
		s.WriteString(*ss[i])
		s.WriteString("\r\n")
	}

	return s.String()
}
