package intcomp

import (
	"encoding/binary"
	"log"
	"testing"

	"gitee.com/haodreams/libs/easy"
)

func TestEncode(t *testing.T) {
	ic := NewSeqComp[uint32](true)
	for i := 0; i < 100; i++ {
		ic.Write(uint32(i))
	}
	data := ic.Bytes()
	log.Printf("% 02X", data)

	ids, err := ic.Decode(data)
	if err != nil {
		log.Fatalln(err)
	}
	for i, id := range ids {
		log.Println(i+1, id)
	}

	n := -1
	data = make([]byte, 4)
	binary.BigEndian.PutUint32(data, uint32((n<<1)^(n>>31)))
	log.Printf("% 02x", data)
	n1 := binary.BigEndian.Uint32(data)
	log.Println(int32((n1 >> 1) ^ -(n1 & 1)))
	n = -2
	binary.BigEndian.PutUint32(data, uint32((n<<1)^(n>>31)))
	log.Printf("% 02x", data)
}
func TestEncodeUint24(t *testing.T) {
	ic := NewSeqComp[Uint24](true)
	for i := 0; i < 100; i++ {
		ic.Write(Uint24(i))
	}
	data := ic.Bytes()
	log.Printf("% 02X", data)

	ids, err := ic.Decode(data)
	if err != nil {
		log.Fatalln(err)
	}
	for i, id := range ids {
		log.Println(i+1, id)
	}

}

func TestDecodeE(t *testing.T) {

	slice := []int64{
		1, 2, 3, 152921504606846975, 100, -1, -2, -3, -4,
	}

	ic := NewIntComp(len(slice))
	for _, i := range slice {
		ic.Write(i)
	}
	bs := ic.Flush()
	log.Println(len(bs), len(slice)*8, easy.BeautifyFloat(float64(len(bs))/float64(len(slice)*8), 2))

	vals, err := ic.Decode(bs)
	log.Println(err, vals)
}
