package main

import (
	"net/http"

	"gitee.com/haodreams/libs/fsx"
)

func main() {
	fx := fsx.New("/files", "./")
	http.Handle(fx.Pattern(), fx)
	http.ListenAndServe(":81", nil)
}
