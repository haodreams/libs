package sock

import (
	"errors"
	"fmt"
	"io"
	"net"
	"strconv"
	"strings"
	"time"
)

// Decimal to integer starting at &s[i0].
// Returns number, new offset, success.
func dtoi(s string, i0 int) (n int, i int, ok bool) {
	n = 0
	for i = i0; i < len(s) && '0' <= s[i] && s[i] <= '9'; i++ {
		n = n*10 + int(s[i]-'0')
	}
	if i == i0 {
		return 0, i, false
	}
	return n, i, true
}

// ParseIPv4 IPv4 address (d.d.d.d).
func ParseIPv4(s string) (ip uint32, err error) {
	var p [4]byte
	i := 0
	for j := 0; j < 4; j++ {
		if i >= len(s) {
			// Missing octets.
			err = errors.New("missing octets")
			return
		}
		if j > 0 {
			if s[i] != '.' {
				err = errors.New("IP string is error")
				return
			}
			i++
		}
		var (
			n  int
			ok bool
		)
		n, i, ok = dtoi(s, i)
		if !ok || n > 0xFF {
			err = errors.New("IP string is error")
			return
		}
		p[j] = byte(n)
	}
	if i != len(s) {
		err = errors.New("IP string is error")
		return
	}
	ip = uint32(p[0])<<24 | uint32(p[1])<<16 | uint32(p[2])<<8 | uint32(p[3])
	return ip, nil
}

// IPPort2String ...
func IPPort2String(ip uint32, port uint16) string {
	return fmt.Sprintf("%d.%d.%d.%d:%d", ip>>24, ip>>16&0xff, ip>>8&0xff, ip&0xff, port)
}

// IP2String ...
func IP2String(ip uint32) string {
	return fmt.Sprintf("%d.%d.%d.%d", ip>>24, ip>>16&0xff, ip>>8&0xff, ip&0xff)
}

// CreateTcpListener ...
func CreateTcpListener(host string) (listener *net.TCPListener, err error) {
	host = FillAddr(host)

	tcpAddr, err := net.ResolveTCPAddr("tcp", host)
	if err != nil {
		return nil, err
	}

	listener, err = net.ListenTCP("tcp", tcpAddr)
	if err != nil {
		return nil, err
	}
	return
}

// CreateTcpConnect ...
func CreateTcpConnect(host string, timeout ...int64) (conn *net.TCPConn, err error) {
	// tcpAddr, err := net.ResolveTCPAddr("tcp", host)
	// if err != nil {
	// 	return nil, err
	// }
	timeoutms := int64(30000)
	if len(timeout) > 0 {
		timeoutms = timeout[0]
		//最少1秒钟
		if timeoutms < 1000 {
			timeoutms = 1000
		}
		//最多2分钟
		if timeoutms > 120000 {
			timeoutms = 120000
		}
	}

	//conn, err = net.DialTCP("tcp", nil, tcpAddr)
	c, err := net.DialTimeout("tcp", host, time.Millisecond*time.Duration(timeoutms))
	if err != nil {
		return nil, err
	}
	conn, ok := c.(*net.TCPConn)
	if !ok {
		err = errors.New("not tcp connect")
		return
	}

	//设置tcp心跳时间
	conn.SetKeepAlive(true)
	conn.SetKeepAlivePeriod(60 * time.Second)
	return
}

// CreateUdpConnect ...
func CreateUdpConnect(address string) (conn *net.UDPConn, err error) {
	addr, err := net.ResolveUDPAddr("udp", address)
	if err != nil {
		return nil, err
	}
	conn, err = net.DialUDP("udp", nil, addr)
	if err != nil {
		return nil, err
	}
	return conn, err
}

func CreateUdpListenAny(address ...string) (conn *net.UDPConn, addr *net.UDPAddr, err error) {
	if len(address) > 0 && address[0] != "" {
		addr, err = net.ResolveUDPAddr("udp", address[0])
		if err != nil {
			return
		}
	}

	conn, err = net.ListenUDP("udp", &net.UDPAddr{IP: net.IPv4zero, Port: 0})
	return
}

// CreateUdpListener ...
func CreateUdpListener(host string) (listener *net.UDPConn, err error) {
	udpAddr, err := net.ResolveUDPAddr("udp", host)
	if err != nil {
		return nil, err
	}

	listener, err = net.ListenUDP("udp", udpAddr)
	if err != nil {
		return nil, err
	}
	return
}

//func read(conn *net.TCPConn, data []byte) (int, error) {
//	return conn.Read(data)
//}

//ReadTimeout ...
/*在指定的时间内,接收指定的字节数, 当没有错误发生时, num = len(data)
* 当有错误发生是num为实际接收的字节数
 */
func ReadTimeout(conn io.Reader, data []byte, argv ...int) (num int, err error) {
	length := len(data)
	num = 0
	mstimeout := 5000
	if len(argv) > 0 {
		mstimeout = argv[0]
	}

	if mstimeout < 20 {
		mstimeout = 20
	}

	if tcpconn, ok := conn.(net.Conn); ok {
		err = tcpconn.SetReadDeadline(time.Now().Add(time.Millisecond * time.Duration(mstimeout)))
		if err != nil {
			return
		}
		for num < length {
			n, err := tcpconn.Read(data[num:]) ///*read(tcpconn, data[num:])*/
			if err != nil {
				return num, err
			}
			if n > 0 {
				tcpconn.SetReadDeadline(time.Now().Add(time.Millisecond * time.Duration(mstimeout)))
				num += n
			}
		}
	} else {
		n := 0
		t := time.Now()
		for num < length && mstimeout > 0 {
			//log.Println(num, length)
			n, err = conn.Read(data[num:])
			//if err != nil && err.Error() == "EOF" {
			if err != nil {
				return num, err
			}
			//log.Println(err, err.Error() != "EOF")
			num += n
			if n == 0 {
				t1 := int(time.Since(t) / time.Millisecond)
				if t1 >= mstimeout {
					err = errors.New("timeout")
					break
				} else if t1+50 > mstimeout {
					time.Sleep(time.Duration(mstimeout-t1) * time.Millisecond)
				} else {
					time.Sleep(time.Millisecond * 50)
				}
				continue
			}
		}
	}
	return
}

//Read ...
/*接收指定的字节数, 当没有错误发生时, num = len(data)
* 当有错误发生是num为实际接收的字节数
 */
func Read(conn io.Reader, data []byte) (num int, err error) {
	length := len(data)
	num = 0
	for num < length {
		n, err := conn.Read(data[num:])
		if err != nil {
			return num, err
		}
		num += n
	}
	return
}

// ReadLenString ...
func ReadLenString(conn io.Reader) (str string, err error) {
	data, err := ReadLenBytes(conn)
	str = string(data)
	return
}

// ReadLenBytes 读取指定长度的字节
func ReadLenBytes(conn io.Reader) (data []byte, err error) {
	data = make([]byte, 2)
	n, err := ReadTimeout(conn, data, 60000)
	if err != nil {
		n = 0
		return
	}
	_ = n
	l := uint16(data[0])<<8 | uint16(data[1])
	data = make([]byte, l)
	n, err = ReadTimeout(conn, data, 60000)
	if err != nil {
		return data[:n], err
	}
	return
}

// WriteLenBytes 写入指定长度的字节
func WriteLenBytes(conn io.Writer, data []byte) (n int, err error) {
	b := make([]byte, 2)
	l := len(data)
	b[0] = byte(uint16(l) >> 8)
	b[1] = byte(l)
	_, err = conn.Write(b)
	if err != nil {
		n = 0
		return
	}
	n, err = conn.Write(data)
	return
}

// WriteLenString ...
func WriteLenString(conn io.Writer, str string) (n int, err error) {
	return WriteLenBytes(conn, []byte(str))
}

// 如果地址是个端口号，自动在端口号前加冒号
func FillAddr(addr string) string {
	addr = strings.TrimSpace(addr)
	if addr == "" {
		return addr
	}
	if strings.Contains(addr, ":") {
		return addr
	}
	_, err := strconv.Atoi(addr)
	if err != nil {
		return addr
	}
	return ":" + addr
}
