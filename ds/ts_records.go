/*
 * @Author: Wangjun
 * @Date: 2023-09-21 19:07:36
 * @LastEditTime: 2023-10-08 10:25:11
 * @LastEditors: Wangjun
 * @Description:recordset 只能存在于dataset中
 * @FilePath: \xr_node_calcd:\go\src\gitee.com\haodreams\libs\ds\ts_records.go
 * hnxr
 */
package ds

import "io"

//时间戳对齐的数据集合
type TsRecords[N number] struct {
	rows []Row[N] //同一秒不同数据id的集合
}

func (m *TsRecords[N]) Rows() []Row[N] {
	return m.rows
}

/**
 * @description: 初始化记录集
 * @param {[]int} ids 当前记录集的行数
 * @param {int} nColumn 当前记录集列数
 * @return {*}
 */
func (m *TsRecords[N]) Setup(ids []int, nColumn int) {
	n := len(ids)
	//设置有多少行
	if cap(m.rows) < n {
		m.rows = make([]Row[N], n)
	} else {
		m.rows = m.rows[:n]
	}
	for i := range m.rows {
		//设置每行有多少列
		m.rows[i].Setup(ids[i], nColumn)
	}
}

func (m *TsRecords[N]) Reset() {
	for i := range m.rows {
		m.rows[i].Reset()
	}
}

func (m *TsRecords[N]) Count() int {
	return len(m.rows)
}

/**
 * @description: 获取一行
 * @param {int} nRow
 * @return {*}
 */
func (m *TsRecords[N]) GetRow(nRow int) *Row[N] {
	if nRow >= len(m.rows) || nRow < 0 {
		return nil
	}
	return &m.rows[nRow]
}

func (m *TsRecords[N]) Value(nRow, nColumn int) *N {
	row := m.GetRow(nRow)
	if row == nil {
		return nil
	}
	return row.Value(nColumn)
}

func (m *TsRecords[N]) SetValue(nRow, nColumn int, v *N) {
	row := m.GetRow(nRow)
	if row == nil {
		return
	}
	row.SetValue(nColumn, v)
}

func (m *TsRecords[N]) WriteTo(w io.Writer, header []byte, writeKey bool) {
	for i := range m.rows {
		w.Write(header)
		m.rows[i].WriteTo(w, writeKey)
	}
}
