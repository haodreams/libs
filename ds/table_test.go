/*
 * @Author: Wangjun
 * @Date: 2023-09-21 19:07:36
 * @LastEditTime: 2023-09-23 19:47:09
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \libs\ds\table_test.go
 * hnxr
 */
package ds

import (
	"bytes"
	"fmt"
	"testing"
	"time"
)

func TestTable(t *testing.T) {
	tab := NewTable[float64]([]string{"A1", "A2"})
	tab.SetRowCount(10)
	tab.Begin = time.Now().Unix()
	tab.Step = 5
	a := 100.0
	tab.SetValue(0, 0, &a)
	tab.SetValue(3, 1, &a)
	nRow := tab.GetRowCount()

	n := tab.GetIndex(tab.Begin + 11)
	tab.SetValue(n, 1, &a)
	buf := bytes.NewBuffer(nil)
	for i := 0; i < nRow; i++ {
		row := tab.GetRow(i)
		row.ID = i + 1
		row.WriteTo(buf, false)
	}
	fmt.Print(buf.String())
}
