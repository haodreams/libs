/*
 * @Author: Wangjun
 * @Date: 2021-08-20 11:25:10
 * @LastEditTime: 2024-05-08 19:34:44
 * @LastEditors: wangjun haodreams@163.com
 * @Description:
 * @FilePath: \golib\structdetail\map_test.go
 * hnxr
 */

package memtable

import (
	"log"
	"testing"
)

type Object struct {
	Parent string `json:"-"`
	Host   string //所属的节点地址
}

type Fan struct {
	Object
}

func TestStruct(t *testing.T) {
	fan := NewStructDetail(new(Fan), "")
	log.Println(fan.Name)
	for _, col := range fan.Columns {
		log.Println(col.Title, col.Name, col.FullName, col.Index, col.Kind)
	}
}
