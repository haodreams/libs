package array

import (
	"log"
	"testing"
)

func TestA(t *testing.T) {
	a := new(Array[int])
	a.Append(1)
	a.Append(2)
	log.Println(*a.ILoc(-1))
}

func TestB(t *testing.T) {
	type B struct {
		BB int
	}
	a := new(Array[*B])
	a.Append(&B{1})
	a.Append(&B{2})
	c := a.ILoc(-3)
	log.Println(c)
}
