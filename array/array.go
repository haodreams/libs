package array

type Array[G any] struct {
	array []G
}

// index localtion
func (m *Array[G]) ILoc(idx int) *G {
	n := len(m.array)
	if idx < 0 {
		idx = n + idx
		if idx > 0 {
			return &m.array[idx]
		}
		return nil
	}
	if idx < n {
		return &m.array[idx]
	}
	return nil
}

func (m *Array[G]) Append(g G) {
	m.array = append(m.array, g)
}

func (m *Array[G]) Array() []G {
	return m.array
}
