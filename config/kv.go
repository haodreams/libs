/*
 * @Author: Wangjun
 * @Date: 2022-05-11 09:59:18
 * @LastEditTime: 2023-09-12 17:21:49
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \golibd:\go\src\gitee.com\haodreams\libs\config\kv.go
 * hnxr
 */
package config

// KV 配置
type KV struct {
	App     string //程序名称
	Name    string `json:"name"`  //--开头 表示分割线
	Value   string `json:"value"` //值
	Desc    string `json:"desc" ` //描述
	Opts    string `json:"opts"`  //可选值 用 竖线分割
	OrderNo int    `json:"order"` // 排序
}

// TableName 表名
func (m *KV) TableName() string {
	return "system_conf"
}

func (m *KV) GetName() string {
	return m.Name
}

func (m *KV) GetValue() string {
	return m.Value
}

func (m *KV) GetDesc() string {
	return m.Desc
}

func (m *KV) GetOptions() string {
	return m.Opts
}
