/*
 * @Author: wangjun haodreams@163.com
 * @Date: 2024-04-01 14:35:50
 * @LastEditors: wangjun haodreams@163.com
 * @LastEditTime: 2024-04-01 14:36:16
 * @FilePath: \libs\config\decode\main.go
 * @Description: //查看加密的数据
 */
package main

import (
	"flag"
	"fmt"

	"gitee.com/haodreams/libs/config"
)

var pdata = flag.String("data", "", "加密后的字符串")
var pass = flag.String("passwd", "", "加密的密钥")

func main() {
	flag.Parse()
	if *pdata == "" {
		fmt.Println("参数错误")
		return
	}
	if *pass != "" {
		config.SetKey(*pass)
	}
	value := config.DecodeValue(*pdata)
	fmt.Println(value)
}
