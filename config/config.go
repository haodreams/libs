package config

// Configer defines how to get and set value from configuration raw data.
type Configer interface {
	String(key string) string //support section::key type in key string when using ini and json type; Int,Int64,Bool,Float,DIY are same.
	Int(key string) (int, error)
	Int64(key string) (int64, error)
	Bool(key string) (bool, error)
	Float(key string) (float64, error)
	DefaultString(key string, defaultVal string) string // support section::key type in key string when using ini and json type; Int,Int64,Bool,Float,DIY are same.
	DefaultInt(key string, defaultVal int) int
	DefaultInt64(key string, defaultVal int64) int64
	DefaultBool(key string, defaultVal bool) bool
	DefaultFloat(key string, defaultVal float64) float64
}

//Setter .
type Setter interface {
	Set(key, val, desc string) error                    //support section::key type in given key when using ini type.
	SetValue(key, val string) error                     //support section::key type in given key when using ini type.
	SetWidthOption(key, val, desc, option string) error //support section::key type in given key when using ini type.
}

//Loader 加载
type Loader interface {
	Load(file interface{}) (conf Configer, err error)
}

//Saver 保存
type Saver interface {
	Save(file ...interface{}) (err error)
}

type KValuer interface {
	GetName() string
	GetValue() string
	GetDesc() string
	GetOptions() string
}

type Getter interface {
	Get(string) KValuer
}
