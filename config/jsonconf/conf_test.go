package jsonconf

import (
	"fmt"
	"testing"

	"gitee.com/haodreams/libs/config"
)

func TestSave(t *testing.T) {
	var kvs []*config.KV
	kv := new(config.KV)
	kv.Name = "test"
	kv.Desc = "demo"
	kv.Value = "100"
	kv.Opts = "100|200"
	kvs = append(kvs, kv)
	kv = new(config.KV)
	kv.Name = "test"
	kv.Desc = "demo"
	kv.Value = "1010"
	kv.Opts = "1010|200"
	kvs = append(kvs, kv)

	conf := new(Conf)
	conf.SetList(kvs)
	conf.Save("test.json")
}

func TestLoad(t *testing.T) {
	conf := new(Conf)
	conf.Load("test.json")
	kvs := conf.List()
	for _, kv := range kvs {
		fmt.Printf("%+v\n", kv)
	}
}
