package posx

import (
	"log"
	"testing"
)

type age struct {
	Age1 int
}

type Pos struct {
	Name int
	ID   int
	Desc int
	age
}

func TestColumn(t *testing.T) {
	line := "ID,Name,Desc,Age1"
	pos := &Pos{}

	l := FormatColumnsPosition(line, pos)
	log.Println(l)

	log.Println(pos)
}
