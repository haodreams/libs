package posx

import (
	"log"
	"reflect"
	"strings"
)

// FormatColumnsPosition 查找指定结构体的字段位置
// 读取每个字段的位置
// pos 必须是结构体的地址
// 小于0是没有找到
func FormatColumnsPosition(titleCols string, pos interface{}) int {
	val := reflect.ValueOf(pos)
	if val.IsNil() {
		log.Println(val.IsNil())
		return -1
	}
	//如果参数是一个指针，并且指针不为空，则v=*v
	if val.Kind() == reflect.Ptr && !val.IsNil() {
		val = val.Elem()
	}

	typ := reflect.TypeOf(pos).Elem()
	for i := 0; i < val.NumField(); i++ {
		if val.Field(i).CanSet() {
			val.Field(i).SetInt(-1)
		}
	}

	line := strings.TrimSpace(titleCols)
	ss := strings.Split(line, ",")
	for i := 0; i < len(ss); i++ {
		ss[i] = strings.TrimSpace(ss[i])
		for j := 0; j < typ.NumField(); j++ {
			if typ.Field(j).Name == ss[i] {
				if val.Field(j).CanSet() {
					val.Field(j).SetInt(int64(i))
				}
			}
		}
	}
	return len(ss)
}

// FormatColumnsPositionNoCase 查找指定结构体的字段位置 不区分大小写
// 读取每个字段的位置
// pos 必须是结构体的地址
// 小于0是没有找到
func FormatColumnsPositionNoCase(titleCols string, pos interface{}) int {
	val := reflect.ValueOf(pos)
	if val.IsNil() {
		log.Println(val.IsNil())
		return -1
	}
	//如果参数是一个指针，并且指针不为空，则v=*v
	if val.Kind() == reflect.Ptr && !val.IsNil() {
		val = val.Elem()
	}

	typ := reflect.TypeOf(pos).Elem()
	for i := 0; i < val.NumField(); i++ {
		if val.Field(i).CanSet() {
			val.Field(i).SetInt(-1)
		}
	}

	line := strings.ToUpper(titleCols)
	ss := strings.Split(line, ",")
	for i := 0; i < len(ss); i++ {
		ss[i] = strings.TrimSpace(ss[i])
		for j := 0; j < typ.NumField(); j++ {
			name := strings.ToUpper(typ.Field(j).Name)
			if name == ss[i] {
				if val.Field(j).CanSet() {
					val.Field(j).SetInt(int64(i))
				}
			}
		}
	}
	return len(ss)
}
