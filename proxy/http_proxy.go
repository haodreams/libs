/*
 * @Author: wangjun haodreams@163.com
 * @Date: 2024-09-24 09:38:39
 * @LastEditors: wangjun haodreams@163.com
 * @LastEditTime: 2024-12-19 18:32:10
 * @FilePath: \libs\proxyhttp\proxy.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package proxy

import (
	"bufio"
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"time"
)

// httpDialer 代理连接转发
type httpDialer struct {
	proxyServer string
	ssl         bool
}

type Dial func(network, raddr string) (c net.Conn, err error)
type ContextDial func(ctx context.Context, network, address string) (c net.Conn, err error)

func DialFunc(uri string) (d Dial, err error) {
	u, err := url.Parse(uri)
	if err != nil {
		return
	}
	switch u.Scheme {
	case "http":
		d = NewHttpProxy(u.Host, false).Dial
		return
	case "https":
		d = NewHttpProxy(u.Host, true).Dial
		return
	case "":
		d = NewHttpProxy(u.Host, false).Dial
		return
	}
	return nil, errors.New("不支持的代理")
}

func ContextDialFunc(uri string) (d ContextDial, err error) {
	u, err := url.Parse(uri)
	if err != nil {
		return
	}
	switch u.Scheme {
	case "http":
		d = NewHttpProxy(u.Host, false).DialContext
		return
	case "https:":
		d = NewHttpProxy(u.Host, true).DialContext
		return
	}
	return nil, errors.New("不支持的代理")
}

func NewHttpProxy(host string, ssl bool) *httpDialer {
	m := new(httpDialer)
	m.proxyServer = host
	m.ssl = ssl
	return m
}

// Dial 代理连接
// addr 别名
func (m *httpDialer) Dial(network, addr string) (c net.Conn, err error) {
	return m.DialContext(context.TODO(), network, addr)
}

// DialContext .
func (m *httpDialer) DialContext(ctx context.Context, network, address string) (c net.Conn, err error) {
	netDialer := &net.Dialer{
		Timeout:   5 * time.Second,
		KeepAlive: 30 * time.Second,
	}
	if m.proxyServer == "" {
		if m.ssl {
			return tls.DialWithDialer(netDialer, network, address, &tls.Config{InsecureSkipVerify: true})
		}
		return netDialer.DialContext(ctx, network, address)
	}
	var conn net.Conn
	if m.ssl {
		d := new(tls.Dialer)
		d.NetDialer = netDialer
		d.Config = &tls.Config{InsecureSkipVerify: true}
		conn, err = d.DialContext(ctx, network, m.proxyServer)
	} else {
		conn, err = netDialer.DialContext(ctx, network, m.proxyServer)
	}
	if err != nil {
		return
	}

	req, err := http.NewRequest("CONNECT", "", nil)
	if err != nil {
		conn.Close()
		return
	}
	req.Host = address
	if err = req.Write(conn); err != nil {
		conn.Close()
		err = fmt.Errorf("写请求错误：%v", err)
		return
	}

	br := bufio.NewReader(conn)

	res, err := http.ReadResponse(br, req)
	if err != nil {
		conn.Close()
		err = fmt.Errorf("响应格式错误：%v", err)
		return
	}

	if res.StatusCode != 200 {
		conn.Close()
		err = fmt.Errorf("响应错误：%v", res)
		return
	}

	c = conn
	return
}
