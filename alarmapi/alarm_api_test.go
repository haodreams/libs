/*
 * @Author: Wangjun
 * @Date: 2023-12-05 16:01:16
 * @LastEditTime: 2023-12-05 16:21:53
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \libs\alarmapi\alarm_api_test.go
 * hnxr
 */
package alarmapi

import (
	"log"
	"testing"
)

func TestA(t *testing.T) {
	Setup("http://10.202.10.13:20080/v1/api/NewEvent")
	e := new(Event)
	e.Owner = "王军"
	e.Group = "test"
	e.Module = "api"
	e.Message = "api test"
	e.Level = 1
	err := SendAlarm(e)
	if err != nil {
		log.Println(err)
	}
}
