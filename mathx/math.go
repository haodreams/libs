package mathx

import "errors"

func Avg(vals []float64) (float64, error) {
	if len(vals) == 0 {
		return 0, errors.New("")
	}
	sum := 0.0
	for _, v := range vals {
		sum += v
	}
	return sum, nil
}

func Max(vals []float64) (float64, error) {
	if len(vals) == 0 {
		return 0, errors.New("")
	}
	max := vals[0]
	for _, v := range vals {
		if max < v {
			max = v
		}
	}
	return max, nil
}

func Min(vals []float64) (float64, error) {
	if len(vals) == 0 {
		return 0, errors.New("")
	}
	min := vals[0]
	for _, v := range vals {
		if min > v {
			min = v
		}
	}
	return min, nil
}
