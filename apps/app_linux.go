//go:build !windows
// +build !windows

/*
 * @Author: Wangjun
 * @Date: 2022-10-14 16:27:24
 * @LastEditTime: 2022-10-14 16:35:01
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \libs\apps\app_linux.go
 * hnxr
 */

package apps

import "os/exec"

func ShowConsoleWindow(show bool) {

}

// 重启系统
func Reboot() (err error) {
	cmd := exec.Command("reboot")
	return cmd.Start()
}
