package routine

// Routine 包含线程的基本方法
type Routine struct {
	isRunning bool //是否在运行
	//funcBeforStart 启动前执行的操作
	funcBeforStart []func() error
	//funcAfterStart 启动后执行的操作
	funcAfterStart []func() error
	//funcBeforStop 停止前执行的操作
	funcBeforStop []func() error
	//funcAfterStop 停止后执行的操作
	funcAfterStop []func() error
}

// AddFuncBeforStart 线程启动前需要执行的操作
func (t *Routine) AddFuncBeforStart(f func() error) {
	if f == nil {
		return
	}
	t.funcBeforStart = append(t.funcBeforStart, f)
}

// NumberOfBeforStart ...
func (t *Routine) NumberOfBeforStart() int {
	return len(t.funcBeforStart)
}

// AddFuncAfterStart 线程启动后需要执行的操作
func (t *Routine) AddFuncAfterStart(f func() error) {
	if f == nil {
		return
	}
	t.funcAfterStart = append(t.funcAfterStart, f)
}

// AddFuncBeforStop 线程停止前需要执行的操作
func (t *Routine) AddFuncBeforStop(f func() error) {
	if f == nil {
		return
	}
	t.funcBeforStop = append(t.funcBeforStop, f)
}

// AddFuncAfterStop 线程停止后需要执行的操作
func (t *Routine) AddFuncAfterStop(f func() error) {
	if f == nil {
		return
	}
	t.funcAfterStop = append(t.funcAfterStop, f)
}

// IsRunning 是否运行
func (t *Routine) IsRunning() bool {
	return t.isRunning
}

// IsGlobalRunning 总开关处于运行状态，且当前线程处于运行状态
func (t *Routine) IsGlobalRunning() bool {
	return t.isRunning && IsRunning()
}

// Start 启动
func (t *Routine) Start() (err error) {
	for _, f := range t.funcBeforStart {
		err = f()
		if err != nil {
			return
		}
	}

	t.isRunning = true
	for _, f := range t.funcAfterStart {
		err = f()
		if err != nil {
			t.isRunning = false
			return
		}
	}
	return
}

// Stop 停止
func (t *Routine) Stop() (err error) {
	for _, f := range t.funcBeforStop {
		err = f()
		if err != nil {
			return
		}
	}

	t.isRunning = false
	for _, f := range t.funcAfterStop {
		err = f()
		if err != nil {
			t.isRunning = false
			return
		}
	}
	return
}
