/*
 * @Author: wangjun haodreams@163.com
 * @Date: 2024-10-20 15:24:40
 * @LastEditors: wangjun haodreams@163.com
 * @LastEditTime: 2025-02-08 09:58:05
 * @FilePath: \libs\numgo\numgo.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package numgo

import (
	"fmt"
	"log"
	"testing"

	"gitee.com/haodreams/libs/easy"
)

func TestList(t *testing.T) {
	a := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13}
	l := New(a)
	log.Println(l.Get(0))
	log.Println(l.Get(-1))
	log.Println(l.Get(2))
	log.Println(l.Get(-2))
	log.Println(l.Slice(2))
	log.Println(l.Slice(-2))
}

func TestSlice(t *testing.T) {
	s := "15291441"
	formatTime(s)
}

func formatTime(t string) {
	// time = time_stamp[:-6] + ':'
	// if int(time_stamp[-6:-4]) < 60:
	// 	time += '%s:' % time_stamp[-6:-4]
	// 	time += '%06.3f' % (
	// 		int(time_stamp[-4:]) * 60 / 10000.0
	// 	)
	// else:
	// 	time += '%02d:' % (
	// 		int(time_stamp[-6:]) * 60 / 1000000
	// 	)
	// 	time += '%06.3f' % (
	// 		(int(time_stamp[-6:]) * 60 % 1000000) * 60 / 1000000.0
	// 	)
	// return time
	buf := easy.NewBufferWithSize(20)
	buf.Reset()
	slice := New([]byte(t))
	hour := slice.Slice(0, -6)
	buf.Write(hour)
	buf.WriteByte(':')
	v := Int(string(slice.Slice(-6, -4)))
	if v < 60 {
		min := slice.Slice(-6, -4)
		sec := fmt.Sprintf("%06.3f", float64(Int(string(slice.Slice(-4))))*60/10000.0)
		buf.Write(min)
		buf.WriteByte(':')
		buf.Write([]byte(sec))
	} else {
		v := Int(string(slice.Slice(-6)))
		min := fmt.Sprintf("%02d", v*60/1000000)
		sec := fmt.Sprintf("%06.3f", float64((v*60%1000000))*60/1000000.0)
		buf.WriteString(min)
		buf.WriteByte(':')
		buf.WriteString(sec)
	}
	log.Println(buf.String())
	log.Println(String(999999999999999999))
}
