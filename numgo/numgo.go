/*
 * @Author: wangjun haodreams@163.com
 * @Date: 2024-10-20 15:24:40
 * @LastEditors: wangjun haodreams@163.com
 * @LastEditTime: 2025-01-05 19:15:46
 * @FilePath: \libs\numgo\numgo.go
 * @Description: 参考python numpy library
 */
package numgo

import (
	"fmt"
	"strconv"
)

// 返回最新的值
func Newest[V any](vs []V) V {
	return vs[len(vs)-1]
}

type List[V any] struct {
	data []V
}

func New[V any](data []V) *List[V] {
	return &List[V]{data: data}
}

func (l *List[V]) Len() int {
	return len(l.data)
}

func (l *List[V]) getIndex(idx int) int {
	if idx < 0 {
		return len(l.data) + idx
	}
	return idx
}

func (l *List[V]) Get(index int) V {
	return l.data[l.getIndex(index)]
}

func (l *List[V]) Set(index int, v V) {
	l.data[l.getIndex(index)] = v
}

func (l *List[V]) Slice(begin int, end ...int) []V {
	begin = l.getIndex(begin)
	if len(end) == 0 {
		return l.data[begin:]
	}
	e := l.getIndex(end[0])
	return l.data[begin:e]
}

func (l *List[V]) Append(v V) {
	l.data = append(l.data, v)
}

func Int(v string) int64 {
	iv, _ := strconv.ParseInt(v, 10, 64)
	return iv
}

func String(v any) string {
	return fmt.Sprintf("%v", v)
}
