/*
 * @Author: wangjun haodreams@163.com
 * @Date: 2024-07-29 11:02:52
 * @LastEditors: wangjun haodreams@163.com
 * @LastEditTime: 2024-10-30 13:44:10
 * @FilePath: \libs\fixedlist\list_test.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package fixedlist

import (
	"log"
	"testing"
)

type t1 struct {
	id int
}

func TestList(t *testing.T) {
	list := NewFixList[t1](5)
	for i := 1; i < 11; i++ {
		tt := new(t1)
		tt.id = i
		list.Push(tt)
		//ts = append(ts, tt)
	}
	ts := list.Queue(0, list.Size())
	for _, tt := range ts {
		log.Println(tt.id)
	}
	log.Println("=====================")
	ts = list.Queue(0, list.Size())
	for _, tt := range ts {
		log.Println(tt.id)
	}
	log.Println("=====================")

	ts = list.Queue(2, 3)
	for _, tt := range ts {
		log.Println(tt.id)
	}
	log.Println("=====================")
	ts = list.Queue(2, 3)
	for _, tt := range ts {
		log.Println(tt.id)
	}

	log.Println("=====================")
	ts = list.Stack(0, list.Size(), true)
	for _, tt := range ts {
		log.Println(tt.id)
	}
	log.Println("=====================", false)
	ts = list.Stack(0, 3)
	for _, tt := range ts {
		log.Println(tt.id)
	}
	log.Println("=====================", false)
	ts = list.Stack(0, 6)
	for _, tt := range ts {
		log.Println(tt.id)
	}

}

func TestListFloat(t *testing.T) {
	list := NewFixedListFloat(5)
	for i := 1; i < 11; i++ {
		list.Push(float64(i))
		//ts = append(ts, tt)
	}
	ts := list.Queue(0, list.Size())
	for _, tt := range ts {
		log.Println(tt)
	}
	log.Println("=====================")
	ts = list.Queue(2, 3)
	for _, tt := range ts {
		log.Println(tt)
	}

	log.Println("===========Stack==========", false)
	ts = list.Stack(0, list.Size())
	for _, tt := range ts {
		log.Println(tt)
	}
	log.Println("==========Stack===========")
	ts = list.Stack(0, 2)
	for _, tt := range ts {
		log.Println(tt)
	}
}

func TestListPop(t *testing.T) {
	list := NewFixedListFloat(10)
	for i := 1; i < 40; i++ {
		v := list.PushPop(float64(i))
		log.Println(v)
	}
}

func TestListFloat2(t *testing.T) {
	list := NewFixedListFloat(5)
	for i := 1; i < 267; i++ {
		list.Push(float64(i))
		// e, err := list.Pop()
		// log.Println(e, err)
	}
	for i := 0; i < 6; i++ {
		e, err := list.Pop()
		log.Println(e, err)
	}
}
