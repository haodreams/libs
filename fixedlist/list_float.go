/*
 * @Author: Wangjun
 * @Date: 2021-08-04 17:56:00
 * @LastEditTime: 2024-11-14 16:12:01
 * @LastEditors: wangjun haodreams@163.com
 * @Description:定长队列
 * @FilePath: \libs\fixedqueue\queue.go
 * hnxr
 */

package fixedlist

import (
	"io"
	"math"
)

type FixedListFloat struct {
	array []float64
	pos   uint //当前队列位置
	cap   uint //最大容量
	size  uint //当前数据个数
}

/**
 * @description: 新建一个定长队列，只有写，没有读的数据
 * @param {int} size
 * @return {*}
 */
func NewFixedListFloat(size int) *FixedListFloat {
	m := new(FixedListFloat)
	m.SetSize(size)
	return m
}

/**
 * @description: 初始化队列的最大值
 * @param {int} maxLines
 * @return {*}
 */
func (m *FixedListFloat) SetSize(maxLines int) {
	if maxLines == int(m.cap) {
		return // 队列长度不变，不做任何操作
	}
	if maxLines < 2 {
		maxLines = 2
	}
	if maxLines > 20000 {
		maxLines = 20000
	}
	m.array = make([]float64, maxLines)
	m.cap = uint(maxLines)
	m.Reset()
}

/**
 * @description: 获取队列数据大小
 * @param {*}
 * @return {*}
 */
func (m *FixedListFloat) Size() int {
	return int(m.size)
}

/**
 * @description: 复位
 * @param {*}
 * @return {*}
 */
func (m *FixedListFloat) Reset() {
	m.pos = 0
	m.size = 0
	for i := range m.array {
		m.array[i] = math.NaN()
	}
}

/**
 * @description: 压入数据
 * @param {*EventLog} e
 * @return {*}
 */
func (m *FixedListFloat) Push(e float64) {
	m.array[m.pos] = e
	m.pos++
	m.size++
	m.pos = m.pos % m.cap
	if m.size > m.cap {
		m.size = m.cap
	}
}

/**
 * @description: 压入数据
 * @param {*EventLog} e
 * @return {*}
 */
func (m *FixedListFloat) Pop() (e float64, err error) {
	if m.size == 0 {
		return math.NaN(), io.EOF
	}

	pos := (m.cap + m.pos - m.size) % m.cap
	e = m.array[pos]
	m.size--
	return
}

// 如果数据满了会自动移出第一个元素，否则不移出第一个元素，返回第一个元素
func (m *FixedListFloat) PushPop(e float64) float64 {
	v := m.array[int(m.pos%m.cap)]
	m.Push(e)
	return v
}

/**
 * @description:求和
 * @param {*}
 * @return {*}
 */
func (m *FixedListFloat) Sum() (sum float64, count int) {
	count = m.Size()
	idx := 0
	for i := 0; i < count; i++ {
		pos := (m.cap + m.pos - uint(i)) % m.cap
		v := m.array[pos]
		if math.IsNaN(v) {
			continue
		}
		sum += v
		idx++
	}
	return sum, idx
}

/**
 * @description: 求平均值
 * @param {*}
 * @return {*}
 */
func (m *FixedListFloat) Avg() (avg float64) {
	avg, count := m.Sum()
	if count > 0 {
		avg = avg / float64(count)
	} else {
		avg = math.NaN()
	}
	return
}

// 先进后出
func (m *FixedListFloat) Queue(begin, limit int) []float64 {
	count := m.Size()
	if count == 0 {
		return nil
	}
	if limit == -1 {
		limit = count
	}
	end := begin + limit
	if end > count {
		end = count
	}
	if end <= begin {
		return nil
	}
	limit = end - begin

	rows := make([]float64, limit)
	ii := 0
	for i := begin; i < end; i++ {
		if ii >= m.Size() {
			break
		}
		pos := ((m.cap + m.pos - m.size) + uint(i)) % m.cap
		p := m.array[pos]
		rows[ii] = p
		ii++
	}

	return rows[:ii]
}

func (m *FixedListFloat) Stack(begin, limit int) []float64 {
	count := m.Size()
	if count == 0 {
		return nil
	}
	if limit == -1 {
		limit = count
	}
	end := begin + limit
	if end > count {
		end = count
	}
	if end <= begin {
		return nil
	}
	limit = end - begin

	rows := make([]float64, limit)
	ii := 0
	for i := begin; i < end; i++ {
		if ii >= m.Size() {
			break
		}
		pos := ((m.cap + m.pos) - uint(i) - 1) % m.cap
		p := m.array[pos]
		rows[ii] = p
		ii++
	}

	return rows[:ii]
}
