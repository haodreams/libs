/*
 * @Author: Wangjun
 * @Date: 2023-02-27 14:48:23
 * @LastEditTime: 2024-12-16 16:53:22
 * @LastEditors: wangjun haodreams@163.com
 * @Description:
 * @FilePath: \xrop2opd:\go\src\gitee.com\haodreams\libs\fixedqueue\queue.go
 * hnxr
 */
package fixedlist

import (
	"encoding/json"
	"io"
)

// 固定大小的队列
type FixList[G any] struct {
	array []*G
	pos   uint //当前队列位置
	cap   uint //最大容量
	size  uint //当前数据个数
}

/**
 * @description: 新建一个固定大小的队列
 * @return {*}
 */
func NewFixList[G any](size int) *FixList[G] {
	m := new(FixList[G])
	m.SetSize(size)
	return m
}

func (m *FixList[G]) Reset() {
	m.pos = 0
	m.size = 0
}

/**
 * @description: 初始化队列的最大值
 * @param {int} maxLines
 * @return {*}
 */
func (m *FixList[G]) SetSize(maxLines int) {
	if maxLines < 2 {
		maxLines = 2
	}
	if maxLines > 1000000 {
		maxLines = 1000000
	}
	if maxLines == int(m.cap) {
		return
	}
	m.array = make([]*G, maxLines)
	m.cap = uint(maxLines)
	m.Reset()
}

/**
 * @description: 获取队列数据大小
 * @param {*}
 * @return {*}
 */
func (m *FixList[G]) Size() int {
	return int(m.size)
}

/**
 * @description: 压入数据
 * @param {*EventLog} e
 * @return {*}
 */
func (m *FixList[G]) Push(e *G) {
	m.array[m.pos] = e
	m.pos++
	m.size++
	m.pos = m.pos % m.cap
	if m.size > m.cap {
		m.size = m.cap
	}
}

/**
 * @description: 压入数据
 * @param {*EventLog} e
 * @return {*}
 */
func (m *FixList[G]) Pop() (e *G, err error) {
	if m.size == 0 {
		return nil, io.EOF
	}

	pos := (m.cap + m.pos - m.size) % m.cap
	e = m.array[pos]
	m.size--
	return
}

func (m *FixList[G]) Queue(begin, limit int, orderAsc ...bool) []*G {
	count := m.Size()
	if count == 0 {
		return nil
	}
	if limit == -1 {
		limit = count
	}
	if limit > count {
		limit = count
	}
	end := begin + limit

	rows := make([]*G, limit)
	ii := 0
	asc := true
	if len(orderAsc) > 0 {
		asc = orderAsc[0]
	}
	if asc {
		for i := begin; i < end; i++ {
			pos := ((m.cap + m.pos - m.size) + uint(i)) % m.cap
			p := m.array[pos]
			rows[ii] = p
			ii++
		}
	} else {
		for i := end - 1; i >= begin; i-- {
			pos := ((m.cap + m.pos - m.size) + uint(i)) % m.cap
			p := m.array[pos]
			rows[ii] = p
			ii++
		}

	}

	return rows[:ii]
}

// 先进后出
func (m *FixList[G]) Stack(begin, limit int, orderAsc ...bool) []*G {
	count := m.Size()
	if count == 0 {
		return nil
	}
	if limit == -1 {
		limit = count
	}
	if limit > count {
		limit = count
	}
	end := begin + limit
	if end > count {
		end = count
	}
	if end <= begin {
		return nil
	}
	limit = end - begin

	rows := make([]*G, limit)
	ii := 0
	asc := true
	if len(orderAsc) > 0 {
		asc = orderAsc[0]
	}
	if asc {
		for i := end - 1; i >= begin; i-- {
			pos := ((m.cap + m.pos) - uint(i) - 1) % m.cap
			p := m.array[pos]
			rows[ii] = p
			ii++
		}
	} else {
		for i := begin; i < end; i++ {
			pos := ((m.cap + m.pos) - uint(i) - 1) % m.cap
			p := m.array[pos]
			rows[ii] = p
			ii++
		}
	}

	return rows[:ii]
}

// Write .
func (m *FixList[G]) Write(b []byte) (n int, err error) {
	e := new(G)
	err = json.Unmarshal(b, e)
	if err == nil {
		m.Push(e)
	}
	n = len(b)
	return
}
