/*
 * @Author: Wangjun
 * @Date: 2023-05-30 10:57:21
 * @LastEditTime: 2024-10-11 14:39:12
 * @LastEditors: wangjun haodreams@163.com
 * @Description:
 * @FilePath: \libs\utils\utils_test.go
 * hnxr
 */
package utils

import (
	"log"
	"testing"
)

func TestMoveDir(t *testing.T) {
	err := Move("E:\\projects\\gcc\\dl61850.linux.v1.0.0.20211011", "E:\\projects\\gcc\\bak")
	log.Println(err)
}
