//go:build !windows

/*
 * @Author: Wangjun
 * @Date: 2023-03-17 13:23:03
 * @LastEditTime: 2023-05-30 13:42:20
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \libs\utils\utils_linux.go
 * hnxr
 */

package utils

import "os/exec"

func ShowWindow(show bool) {

}

// 重启系统
func Reboot() (err error) {
	cmd := exec.Command("reboot")
	return cmd.Start()
}
