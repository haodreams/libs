package wnd

import (
	"flag"
	"os"

	"gitee.com/haodreams/libs/utils"
)

var varWindows = flag.Bool("show", false, "Show or hide windows.")

func init() {
	for _, s := range os.Args {
		if s == "-show" {
			return
		}
	}
	utils.Hide()
}
